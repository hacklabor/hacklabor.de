---
title: Vortrag - Basics der Elektronik Elektrotechnik
tags: hacklaborevent technik
author: Thomas
---
# Vortrag: Basics der Elektronik Elektrotechnik
Wer mehr über die technischen Zusammenhänge von Widerstand, Strom, Spannung und Leistung
im Gleichstromkreis erfahren möchte, kommt am 26.09.2019 um 19.00 Uhr ins Hacklabor.

![](/assets/blog/passive_Bauelemente/IMG_20161121_194418.jpg)

_Voraussetzung_

* Mathematik Klasse 7
* Taschenrechner
* Spaß am lernen

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)