---
title: Workshop
tags: workshop spaß public openhouse Esp
author: ThomasK
---
# asynchroner Webserver auf dem ESP
## Donnerstag den 30.04.2020, Start 18.00Uhr 
![Webserver auf dem ESP](/assets/blog/ESPalsWebserver/teaser.jpg)



## Zum Workshop möchten wir euch recht herzlich einladen.



- asyncroner Webserver mit html und css 
- Einrichten und Übertragen der html und css files
- Datenaustausch zwischen ESP und Browser
- Speicherung der Daten auf dem ESP

Die Veranstaltung ist wie immer kostenfrei.
Wo?   Online Youtube [https://youtu.be/pNXX9RnF3iY](https://youtu.be/pNXX9RnF3iY)

### Voraussetzung:
- VSCode + PlattformIO 
- Esp8266 zum testen, wenns geht mit einer LED 

 





  
        
PS: [So kommst du zu uns](/standort/)