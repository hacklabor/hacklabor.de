---
title: Hardware Spenden.
tags: Spende, DIZ, Hardware 
author: Ronald, Daniel, moe
---

# Computern neues Leben einhauchen und Menschen helfen

![Image01](/assets/blog/2022-04-02-HardwareSpenden/image.jpg)

Verstaubt bei dir ein Notebook oder steht ein PC ungenutzt rum, weil du nur noch Smartphone und Tablet nutzt? 

Falls du dich davon trennen kannst, würden wir damit gerne Gutes tun. Wir sammeln Hardware und stellen sie aufbereitet bedürftigen Menschen zur Verfügung.

 Zurzeit unterstützen wir das [Digitale Innovationszentrum](https://www.schwerin.de/wirtschaft/digitales-innovationszentrum/) bei deren Bemühungen ukrainischen Geflüchteten zu helfen.

Du kannst Deine Hardware direkt dort abgeben im: 

Digitale Innovationszentrum Perzinahaus
Wismarsche Str. 144, 19053 Schwerin

oder bringst die Sachen zu uns ins Hacklabor. 
Wer mag bekommt noch eine Führung durch das Hacklabor. 

Das Hacklabor ist der Schweriner Hackspace und eingetragener gemeinnütziger Verein. Ihr findet uns im TGZ, Hagenower Straße 73, 19061 Schwerin oder auf www.hacklabor.de.



Am besten du schaust jetzt gleich mal, ob du Hardware entbehren kannst und bringst sie zeitnah vorbei. Bis auf ganz wenige Ausnahmen sind wir Dienstag & Freitag Abend im Hacklabor. 

Wir freuen uns auf jede Spenderin / jeden Spender oder einfach darauf euch kennenzulernen. :-)