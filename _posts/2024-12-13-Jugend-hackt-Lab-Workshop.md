---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Workshop: Plätzchenformen und Weihnachtsdeko selbst gestalten
![Workshop Weihnachtshackerei](/assets/blog/2024-12-13-Jugend-hackt-Workshop.jpg)

Hast du Lust, deiner Kreativität freien Lauf zu lassen und dabei in die spannende Welt des 3D-Drucks einzutauchen? In unserem Workshop „In der Weihnachtshackerei“ kannst du genau das erleben! 🛠️✨

Gemeinsam gestalten wir einzigartige Plätzchenformen und kreative Weihnachtsdeko – von der ersten Idee bis zum fertigen 3D-gedruckten Ergebnis. Du lernst, wie du eigene Designs erstellst und sie mit moderner Technik zum Leben erweckst. Ob du eine coole Schneeflocke, einen witzigen Weihnachtsbaum oder dein ganz persönliches Motiv gestalten möchtest – alles ist möglich! 🎄❄️🍪

Der Workshop ist für alle Jugendlichen, die Lust auf Technik, Kreativität und Spaß haben – egal, ob du schon Vorerfahrung hast oder nicht. Wir zeigen dir Schritt für Schritt, wie alles funktioniert.

Eine Anmeldung ist nicht erforderlich – komm einfach vorbei und mach mit!

Wir freuen uns darauf, mit dir zu basteln, zu hacken und zu designen – und am Ende vielleicht die coolste Plätzchenform oder Deko des Jahres in der Hand zu halten. 🖌️🎅

Bis bald in der Weihnachtshackerei! 🦙

#### Wann?
14.12.2024 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

