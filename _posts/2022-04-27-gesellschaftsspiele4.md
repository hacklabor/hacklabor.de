---
title: Gesellschaftsspiele Nachmittag im Mai
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Gesellschaftsspiele Nachmittag im Hacklabor

Nach der langen Corona Pause möchten wir mit euch wieder zusammen spielen. Deswegen laden wir euch am Samstag den 14. Mai 2022 ab 14 Uhr zu uns in das Hacklabor ein. 
Es sind reichlich Spiele vorhanden aber wie immer freuen wir uns wenn ihr euer Lieblingsspiel mitbringt.

Die Veranstaltung ist wie immer kostenfrei.

Was?  - Gesellschaftsspiele Nachmittag im Hacklabor  
Wann? - Samstag 14.05.2022 ab 14 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
PS: [So kommst du zu uns](/standort/)
