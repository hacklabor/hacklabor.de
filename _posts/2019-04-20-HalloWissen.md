---
title: Hallo Wissen
tags: event wissen hallo vorträge public openhouse
author: Christoph 'ovnn'
---
# Hallo Wissen - Frühlingsfest des Hacklabors
Am Samstag den 25.5.2019 von 13 bis 21 Uhr laden wir zu unserem Frühlingsfest "Hallo Wissen" ein. 
An diesem Tag wird es bei uns Vorträge zu verschiedenen Themen, Lasercutter und 3D Druck in Aktion sowie kleine Bastelprojekte geben. Für das leibliche Wohl ist natürlich auch gesorgt.
[![Hallo Wissen](/assets/blog/halloWissen.png)](https://www.facebook.com/events/326757258255303/)


Die Veranstaltung ist bis auf das leibliche Wohl kostenfrei.

Was?  - Frühlingsfest für Neugierige  
Wann? - Samstag 25.5.2019 13 bis 21 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
## Vortragsprogramm:

15 und 19 Uhr - Aus für Windows7 -  was tun?  Vortragender: Christoph  
16 und 18 Uhr - Speichermedien - eine kurze Einführung. Vortragender: claus  
17 und 20 Uhr - Polizeigesetz - Vortragender: Micha  

PS: [So kommst du zu uns](/standort/)

