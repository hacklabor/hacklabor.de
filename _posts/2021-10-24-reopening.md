---
title: Öffnung mit 2G-plus-Regelung
tags: corona reopening
author: Gerd
---

# Öffentliche Treffen und Veranstaltungen finden wieder im Hacklabor statt
## Besuch im Space geimpft oder genesen sowie zusätzlich getestet möglich

Das Hacklabor öffnet sich wieder für externe Besucher zu Veranstaltungen und zu den öffentlichen Treffen am Dienstag und Freitag.

**Dabei gilt grundsätzlich die [2G-plus-Regel](https://www.regierung-mv.de/corona/Corona-Beschluesse/) für alle Anwesenden.**

Gäste werden darum gebeten, einen entsprechenden Nachweis beim Eintreten vorzuzeigen und ein Formular zur Kontaktnachverfolgung auszufüllen.
Darüber hinaus sind selbstverständlich die jeweils gültigen Corona-Regeln in Abhängigkeit von der [aktuellen Gefahrenstufe einzuhalten](https://www.mv-corona.de/).

Mit dieser Maßnahme möchten wir den Space wieder zugänglicher machen und gleichzeitig einen gemeinsamen, sicheren Raum schaffen, der den Mitgliedern und Gästen ein bisschen Normalität zurück gibt.

Wir freuen uns auf euch!

**UPDATE 30.11.2021:** Da die Sieben-Tage-Hospitalisierungsinzidenz drei Tage in Folge landesweit den Schwellenwert 9 überschritten hat (Schutzstufe Rot), gelten ab Mittwoch (1. Dezember) in Mecklenburg-Vorpommern verschärfte Kontaktbeschränkungen und Corona-Maßnahmen. Aus diesem Grund gilt ab sofort auch die 2G-plus-Regel für alle Anwesenden im Hacklabor. Die entsprechenden Stellen in diesem Post wurden aktualisiert.

Weitere Informationen dazu gibt es [hier](https://www.mv-corona.de/news/ab-mittwoch-schutzstufe-rot-mv-das-sind-die-regeln).
