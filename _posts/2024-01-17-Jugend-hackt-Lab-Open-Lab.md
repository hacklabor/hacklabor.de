---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---
# Open Lab
![OpenLab](/assets/blog/2024-01-17-Jugend-hackt-Lab-Open-Lab.jpg)

Hier bestimmst du das Programm. Egal ob du Hilfe beim Programmieren brauchst, LEDs zum leuchten bringen, Löten lernen, einen Roboter bauen, etwas in der Holzwerkstatt konstruieren oder vielleicht den 3D-Drucker und den Lasercutter ausprobieren möchtest – wir sind an diesem regelmäßigen Termin da, um mit dir gemeinsam deine Ideen zu verwirklichen.

Und falls du noch keine eigene Idee hast, finden wir bestimmt gemeinsam ein spannendes Projekt für dich. 💡
Schreib uns gerne eine E-Mail, wenn du eine Frage oder eine Idee hast, die vielleicht etwas Vorbereitung benötigt.

Deine Vorkenntnisse spielen dabei überhaupt keine Rolle und alle Jugendlichen zwischen 12 und 18 Jahren sind herzlich willkommen!

Eine Anmeldung ist nicht erforderlich. Komm einfach um 14 Uhr vorbei und lass uns mit Code die Welt verbessern! 🦙


#### Wann?
20.01.2024 - um 14 Uhr bis 18 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

