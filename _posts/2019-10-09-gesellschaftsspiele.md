---
title: Gesellschaftsspiele Nachtmittag
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Gesellschaftsspiele Nachtmittag im Hacklabor

Gesellschaftsspiele bei Kaffee und Kuchen. Spielt mit uns in einer technischen Räumlichkeit ganz technikfreie Spiele wie Würfel, Brett und Gesellschaftsspiele am Sonntag den 10.11.19 ab 14 Uhr.
Bringt aber gerne euer bevorzugtes Spiel mit so haben wir mit den bereits vorhanden mehr Auswahl was gespielt werden kann. Die Mehrheit entscheidet dann welche gespielt werden.

Die Veranstaltung ist kostenfrei.

Was?  - Gesellschaftsspiele Nachtmittag  
Wann? - Sonntag 10.11.2019 ab 14 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
PS: [So kommst du zu uns](/standort/)

