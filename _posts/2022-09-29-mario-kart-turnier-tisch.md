---
title: Mario Kart Turnier im Tisch
tags: mario kart turnier tisch spielen
author: moe
---

# Mario Kart Turnier im Tisch am 14. Oktober 2022

Bist du der schnellste?
Stelle dein können unter Beweis am Gaming Friday!

Am 14. Oktober 2022 findet zum wiederholten Mal das Mario Kart Turnier im Tisch, Martinstraße 11, 19053 Schwerin statt. Ihr seid herzlich eingeladen euer Geschick erneut auf die Probe zu stellen und euch mit ebenbürtigen Gegnern zu messen. Doch am Ende des Abends kann nur einer der schnellste und fingerfertigste sein!

Also kommt vorbei. Einlass ist um 19 Uhr und ab 19:30 Uhr beginnen wir mit dem Battle. Egal ob Kontrahent oder anfeuernder Zuschauer, jeder ist willkommen.

Eine kostenlose Registrierung im Vorfeld könnt auf der Seite vom Tisch ausfüllen. https://www.tisch.space/events/gaming-friday-4
tisch
