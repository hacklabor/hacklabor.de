---
title: Happy Birthday Hackspace Schwerin e. V.
tags: Birthday, Offene Tür, event, public, open
author: Gerd
---

Das Hacklabor wird 7 Jahre alt und das möchten wir gemeinsam mit euch (rein-)feiern!

Zur Feier des Tages führen wir am Samstag, den 25.02., ab 18 Uhr immer mal wieder durch das Hack und das Labor, erzählen Geschichten von früher, stellen ein paar unserer Projekte vor und spielen eine Runde Hacker Jeopardy. Dabei ist natürlich auch für das leibliche Wohl gesorgt.

Die Mate ist schon kalt gestellt und wir freuen uns auf euren Besuch!

#### Wann?
25.02.2023 - ab 18 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

PS: [So kommt ihr zu uns](/standort/)

🚧 Bitte beachte 🚧: Zur Zeit ist die Zufahrt über die Hagenower Str. gesperrt. Zu Fuß oder mit dem Fahrrad kommst du rechts am Haus 1 vom TGZ vorbei und musst einmal rum gehen. Mit dem Auto erreicht ihr uns über die Mettenheimerstraße.

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

