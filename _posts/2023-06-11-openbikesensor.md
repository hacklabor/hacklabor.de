---
title: OpenBikeSensor in Schwerin
tags: Fahrrad workshop OpenBikeSensor
author: Christoph 'ovnn'
---

Der OpenBikeSensor erreicht nun auch Schwerin. Der OpenBikeSensor wird an das Fahrrad montiert und zeichnet während der Fahrt Überholvorgänge von Autofahrenden auf, die danach zur zentralen Auswertung an ein Portal übermittelt werden. Dort entsteht dann mit den gesammelten Werten eine grafische Karte von Schwerin, die Problemstellen aufzeigt. 
![OpenBikeSensor in Schwerin im einsatz](/assets/blog/obs.jpg)

Du radelst durch die Stadt, dann hilf doch mit, um eine Aussagekräftige Karte zu erstellen. Hier kannst du dich für einen eigenen OpenBikeSensor Workshop im Hacklabor registrieren: https://forms.gle/2t6cj81KFaAaeuMs5


Mehr Infos zum OpenBikeSensor findest du unter [OpenBikeSensor](https://www.openbikesensor.org/)
