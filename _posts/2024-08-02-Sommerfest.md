---
title: Sommerfest im Hacklabor
tags: sommerfest, Offene Tür, event, public, open
author: Gerd
---

![Sommerfest](/assets/blog/2024-08-02-Sommerfest_2024.jpg)

Die Sonne brennt, aber unsere Mate ist schon kalt gestellt: Am **10.08. ab 13:37 Uhr** öffnen wir unsere Türen für alle zum großen Sommerfest. Lasst uns gemeinsam die Hitze hacken und einen coolen Tag verbringen!

**Das erwartet euch:**

- Führungen durch unseren Space

- Nette Gespräche und Austausch in lockerer Atmosphäre

- **Um 16 Uhr**: Spannender Vortrag "Router im 'Selbstbau'"

- **Ab 18 Uhr**: Hacker Jeopardy - das nerdige Quizformat mit jeder Menge Spaß

- Zum Ausklang: Kultiger Hacker-Filmabend für alle Nachteulen

Für kühle Getränke und leckeres Essen ist natürlich gesorgt. Ob drinnen oder draußen - wir haben für jeden Sonnenstand den passenden Platz.

Wir freuen uns riesig auf euren Besuch! Kommt vorbei und lasst uns zusammen einen *exzellenten* Tag erleben!

#### Wann?
10.08.2024 - ab 13.37 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

PS: [So kommt ihr zu uns](/standort/)

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

