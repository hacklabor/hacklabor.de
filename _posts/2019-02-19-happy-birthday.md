---
title: Happy Birthday Hackspace Schwerin e. V.
tags: Birthday, Offene Tür
author: Micha
---

Stellt schon mal die Mate kalt! Am 26.02. vor 3 Jahren wurde der Verein zum Hacklabor gegründet.

Zur Feier des Tages führen wir ab 18 Uhr immer mal wieder durch das Hack und das Labor, erzählen Geschichten von früher, spinnen Utopien und hören uns deine Verschwörungstheorien an. 

Wie eigentlich immer ist der Eintritt frei und Getränke sind vorhanden.

## Details

Dienstag 26.02.2019 ab 18:00  
im [Hacklabor](https://hacklabor.de/standort/)
