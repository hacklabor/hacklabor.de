---
title: Wettbewerb für Schüler:innen und Studierende
tags: event challenge wettbewerb technolympiade tgz
author: Gerd
---

# Technolympiade 2023

Die Technolympiade ist ein Wettbewerb für Schüler:innen der Klassen 8 bis 13 und Studierende, welcher von der ATI Westmecklenburg gemeinsam mit Einrichtungen des TGZ organisiert wird. In diesem Jahr bereits zum 17. Mal. Es sind neben Aufgaben der Fachgebiete Mathematik, Physik und Informatik, praktische Stationen zu bewältigen, die Geschick und logisches Denken erfordern. Die Technolympiade ist eine gute Gelegenheit, das eigene Wissen und die eigenen Fähigkeiten zu erproben. Außerdem gibt es attraktive Preise zu gewinnen. 🏅

Das Hacklabor hat auch dieses Jahr wieder eine Station des Wettbewerbs konzipiert und freut sich auf deinen Besuch. Bei unserer Herausforderung wird dir diesmal buchstäblich ein Licht aufgehen! 💡

[Hier geht’s zur Anmeldung](https://www.technolympiade.de/)

Wir freuen uns auf dich! 

#### Wann?
23.09.2023 - 10 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin


