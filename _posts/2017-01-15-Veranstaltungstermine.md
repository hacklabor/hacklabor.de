---
title: Forecast Veranstaltungen 
tags: hacklaborevent
author: Thomas
---
### Binäre Zahlenwerte.Vom Bit zum Real bzw Float

#### 29.01.2017 14:00 Uhr Dauer ca. 2-3 h

* Aufbau von Binären Zahlen
* Konvertierung
* Addition, Substraktion, Multiplikation
* logische Operationen 


### Layer 1 Kommunikation.....da wo Information zu Elektronen und Elektronen zu Information wird. (RS232, RS458, I2C, SPI, 4...20mA, 0-10V) 

#### 05.02.2017 14:00 Uhr Dauer ca. 2h

* Unterschiede und Gemeinsamkeiten
* Pegel
* Syncronisation
* Codierung

### Passive Bauelemente Teil 2 Kondensatoren 

#### 19.02.2017 14:00 Uhr Dauer ca. 2h

* Grundlagen Aufbau
* Parrallelschaltung
* Reihenschaltung
* Verhalten im Gleichstromkreis
* Verhalten im Wechselstromkreis 

### Passive Bauelemente Teil 3 Induktivitäten

#### 05.03.2017 14:00 Uhr Dauer ca. 2h

* Grundlagen Aufbau
* Parrallelschaltung
* Reihenschaltung
* Verhalten im Gleichstromkreis
* Verhalten im Wechselstromkreis

### Datenblätter von Bauelementen verstehen und deuten 

#### 19.03.2017 14:00 2-3h Uhr

###### an folgenden Beispielen 

* LED
* WS8212b (RGB LED)
* Schaltkreis ULN2003
* Arduino UNO Microcontrollers Atmega 328

###  Grundlagen der digitalen Schaltungstechnik von der Cpu zum Transistor

#### 08.04.2017 14:00 Uhr Dauer ca. 4h

* ALU
* Register
* Addierer
* FlipFlop
* UND, ODER, NAND, NOR, XOR 

### Mein erstes Atmel Assembler Programm

#### 09.04.2017 14:00 Uhr Dauer ca. 4h 

* Aufbau Atmel Controller
* Speicherverwaltung
* Stack
* Pointer
* Statusregister
* Timer
* Programmverarbeitung
* Wir schalten mit einem Taster eine LED EIN
* Wir lassen eine LED blinken

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)