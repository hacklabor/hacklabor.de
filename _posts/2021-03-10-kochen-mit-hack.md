---
title: Kochen im Hacklabor
tags: workshop public cooking 
author: Sebastian
---
# Macht doch mal was mit Hack!
## Freitag den 12.03.2021, Start 20.00Uhr

Lange wurde es gefordert.
"Macht doch mal was mit Hack" haben sie gesagt.

OK, machen wir.

Dieses mal kochen wir mit euch zusammen Spaghetti Bolognese.

Ihr könnt live auf [Twitch](https://www.twitch.tv/hacklabor) dabei sein und mitkochen.

Falls ihr mitmachen wollt, hier die Zutaten:

- 1Pkg Spaghetti
- 1kg Rinderhack
- 4 Zwiebel
- 4 Knoblauch Zehen
- 1 Tube Tomatenmark
- 1 Packung passierte Tomaten
- 1 pkg Suppengrün
- Zucker
- Essig
- Öl
- Pfeffer und Salz

Unser Koch: Th0m4s

Ihr könnt natürlich auch andere Varianten kochen. - Lasst uns am Ergebnis teilhaben und postet ein Bild auf Twitter mit dem Hashtag #kochenmitdemhacklabor

Danach seid ihr herzlich in die [Hacklabor World](http://world.hacklabor.de) eingeladen um über Dinge zu fachsimpeln oder einfach nur zum Quatschen.

**Kommt vorbei, wir freuen uns auf euch!**


### Diese Gerichte gab es schon:

- 05.03.2021 - [Erbsensuppe](https://hacklabor.de/2021/03/kochen-im-hacklabor/)
- 24.02.2021 - [Soljanka](https://hacklabor.de/2021/02/kochen-soljanka/)
- 17.02.2021 - [Gulasch](https://hacklabor.de/2021/02/kochen-und-treffen/)

 