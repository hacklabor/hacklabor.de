---
title: Weihnachtsbasteln [Update]
tags: hacklaborevent weihnachten basteln löten led arduino
author: Murdoc
---
![Weihnachtsbaum](/assets/blog/2016-12-01-weihnachtsbasteln/xmas_tree.jpg)<br>
An alle weihnachtlich gestimmten Technik-Interessierten:

Das Hacklabor will mit Euch eine neue Tradition gründen und lädt zum ersten “Weihnachtsbasteln” ein. Stimmung schaffen wir durch das Fertigen von Lauflichtern und Bunte-LED-Weihnachtsbäumen, die leicht zu löten sind. Wem das zu einfach ist, kann sich gern der Herausforderung eines LED Cubes stellen, welcher mit ein Arduino programmiert werden muss.
<br><br>
Wann:&nbsp;&nbsp;Samstag 10. Dezember 2016<br>
Wo:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;in unseren Räumen
<br><br>
Und für alle jetzt noch Unentschlossenen: Es wird Kuchen geben!!!eins11

## Update

Es gibt zusätzlich noch ein LED-Weihnachtsglocken-Kit zum löten sowie ein kleines Glücksrad!

![Kits](/assets/blog/2016-12-01-weihnachtsbasteln/kits.jpg)<br>

Wir freuen uns auf Euch! Euer Team vom Hacklabor
