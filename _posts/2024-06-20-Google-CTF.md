---
title: Google CTF
tags: event
author: sebastian
---
# Hacking Time - Google CTF

![](/assets/blog/2024-06-20-google-ctf/240619-Google-CTF-sm.jpg)


Die Welt der Cybersecurity ist faszinierend und herausfordernd zugleich. 
Für alle, die sich für dieses spannende Feld interessieren und ihre 
Fähigkeiten auf die Probe stellen möchten, bietet das [Google CTF](https://capturetheflag.withgoogle.com/) eine großartige Gelegenheit. Kommt vorbei, macht mit und stellt euch der Herausforderung!

Das [Google CTF](https://capturetheflag.withgoogle.com/) ist nicht einfach. 
Tatsächlich ist "herausfordernd" das richtige Wort, um die Komplexität und 
die Tiefe der gestellten Aufgaben zu beschreiben. Aber genau das macht den 
Reiz aus! Wer sich den Herausforderungen stellt, hat die Möglichkeit, 
nicht nur seine Fähigkeiten zu testen, sondern auch eine Menge dazuzulernen.

Du kannst neue Techniken und Tools entdecken, deine Problemlösungsfähigkeiten 
verbessern und dich mit anderen Teilnehmern austauschen. Egal, ob du ein 
Anfänger oder ein erfahrener Hacker bist, das Google CTF bietet für 
jeden etwas.

Wir starten am Freitagabend um 19.00 Uhr. Um 20.00 Uhr geht es dann richtig 
los. Der Abend beginnt mit einem 5-minütigen Talk, der dich in die 
CTF-Thematik einführt und dir einen Vorgeschmack auf die bevorstehenden 
Aufgaben gibt.

Am Samstag geht es dann um 10.00 Uhr weiter mit einem gemeinsamen Frühstück. 
Danach beginnen die Challenges, bei denen du dein Können unter Beweis 
stellen kannst.

#### Sei dabei!

Wenn du dich für Cybersecurity interessierst, ist die Google CTF ein Muss. 
Nutze die Gelegenheit, um zu lernen, dich auszutauschen und Spaß zu haben. 
Markiere dir den Freitagabend und Samstag im Kalender und sei bereit für die 
Herausforderung!

Wir sehen uns im Hacklabor beim [Google CTF](https://capturetheflag.withgoogle.com/)!


#### Wann?
21.06.2024 - ab 19:00 Uhr  
22.06.2024 - ab 10:00 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung wie immer kostenlos. Spenden für den Verein sind erwünscht.


