---
title: Chaostreff im Hacklabor
tags: ccc chaostreff
author: micha
---

Wer jetzt bei dem Wort Chaos an den [Chaos Computer Club](https://www.ccc.de/de/) denkt, ist genau richtig. [Chaostreffs](https://www.ccc.de/de/club/chaostreffs) sind lockere Zusammentreffen von Hackern, die sich dem CCC nahefühlen. Für viele von uns aus dem Hacklabor ist diese Organisation eine unabdingbare Institution. Deshalb möchten wir anderen chaosnahen Menschen die Möglichkeit geben, sich zu treffen und auszutauschen.

Der hiesige Chaostreff wird auf den Namen "Chaos Schnack Schwerin (CSS)" hören und sich Dienstags ab 19 Uhr treffen. Du bist herzlich eingeladen dazuzukommen.

PS: [So kommst du zu uns](https://hacklabor.de/standort/) 