---
title: Lokalzeitung über Hacker, Cracker und das Hacklabor
tags: hacker cracker hacklabor
author: Micha
---

Vor kurzem berichtete die [Schweriner Volkszeitung über das Hacklabor](https://www.svz.de/lokales/schwerin/artikel/hacklabor-schwerin-erklaert-was-hacken-eigentlich-bedeutet-42845278) ([via 12ft.io](https://12ft.io/proxy?q=https%3A%2F%2Fwww.svz.de%2Flokales%2Fschwerin%2Fartikel%2Fhacklabor-schwerin-erklaert-was-hacken-eigentlich-bedeutet-42845278)). Solche Artikel sind immer eine tolle Möglichkeit, Menschen auf uns und unsere Arbeit aufmerksam zu machen. Bei diesem Artikel war der Aufhänger leider nicht unsere Arbeit sondern eine Aussage, die man als vermeintliche Besserwisserei abtun könnte. 

"Das, was die meisten Menschen unter Hacking verstehen, werde in Fachkreisen Cracking genannt." fasst die SVZ die Aussagen von Josef Claus zusammen, läßt aber bereits durch die Formulierung der Überschrift "Warum der Begriff Hackerangriff falsch sein soll" Zweifel daran aufkommen. IT-Experten des Datenverarbeitungszentrums MV (DVZ) werden mit "Cracks sind meist sehr gezielt eingesetzte Schadprogramme, beispielsweise um Lizenzen oder auch einen Kopierschutz zu umgehen.“ zitiert und lassen die Aussage von Josef Claus in keinem guten Licht erscheinen. 

**Wer hat Recht und warum ist es uns wichtig auf diese Frage einzugehen?**

Ohne Spezifikationen geht in der IT nichts. Wen wundert es also, außer die Experten beim DVZ, dass bereits in den 1960er Jahren Begriffe der heutigen Hackerkultur definiert wurden. In dem sogenannten [Jargonfile](http://www.catb.org/~esr/jargon/html/index.html) werden auch die Begriffe Hacker und Cracker definiert. Umgangssprachlich werden diese Definitionen tatsächlich von den Wenigsten bewusst genutzt. Genauso wie die Meisten lieber Tempo als Papiertaschentuch oder Rigipsplatte zu Gipskartonplatten sagen. Ein weiterer Versuch gute von bösen Hackern zu unterscheiden ist die Einteilung nach White-, Grey- und Blackhat Hackern. Zugegeben, auch das wird Otto und Helga Normal überfordern.

Ein Großteil der Mitglieder des Hacklabor sieht sich als Teil der Hackerkultur. Für alle ist damit die Hackerethik unzertrennbar verbunden. Wir werden lieber als diejenigen wahrgenommen, denen die Sicherheit von Systemen und Daten am Herzen liegt. Die Unterteilung in Hacker und Cracker könnte uns bei der Abgrenzung zu Kriminellen helfen.

Für eine korrekte Wiedergabe der Hackerethik sei [dieser Beitrag des CCC](https://www.ccc.de/hackerethik) und für weiterführende Informationen [dieser Wikipedia-Artikel zum Thema Cracker](https://de.wikipedia.org/wiki/Cracker_(Computersicherheit)) empfohlen.

Das man im Hacklabor nicht nur Computer kreativ nutzen kann, sondern zum Beispiel auch Holz in unserer Werkstatt bearbeiten kann, kommt in dem Artikel unserer Meinung nach richtig gut rüber. Wer sich selbst ein Bild von uns und dem Hacklabor machen möchte, ist herzlich eingeladen dienstags und freitags ab 19 Uhr im TGZ vorbeizuschauen.
