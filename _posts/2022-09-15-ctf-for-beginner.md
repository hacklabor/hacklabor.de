---
title: CTF für Beginner
tags: ctf workshop howto beginner 
author: Sebastian
---

# Workshop: Einführung in CTF Hacking am 24.09.2022

Habt ihr euch schon immer gefragt wie man Einblicke in den Bereich IT Security bekommt?

Eine gute Möglichkeit können CTF Hacking Challenges sein. Dabei muss man Herausforderungen nach dem Prinzip
"Capture the Flag" lösen und eine versteckte Flagge finden.  
Entsprechende Challenges sind in verschiedene Kategorien unterteilt.

Seid ihr neugierig?

Am 24.09.2022 ab 14 Uhr geben wir Anhand des DownUnderCTF im Rahmen eines Workshops Einblicke in OSINT, REV, PWN, Crypto und andere.  
Der Workshop richtet sich an Anfänger um einen Einstieg zu bekommen.

Wir freuen uns auf dich!

### Links
[DownUnderCTF](https://downunderctf.com/)  
[Hacklabor CTF Time Team Page](https://ctftime.org/team/42892)  
[CTF Time](https://ctftime.org/)

#### Wann?
24.09.2022 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.
