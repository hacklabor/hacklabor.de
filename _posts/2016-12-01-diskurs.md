---
title: Die Auswirkungen der technischen Automatisation auf die Gesellschaft
tags: hacklaborevent gesellschaft diskussion
author: Thomas
---
Wer gerne mit diskutieren möchten ist am 04.12.2016 um 15.00 Uhr in den Räumlichkeiten des Hacklabors
dazu eingeladen.

Zum Ablauf 
 
* Kurze Einführung in das Thema anhand von Filmdokumenten
* angeregte Diskussion bei Kaffee und Kuchen

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)