---
title: "Unser erstes Jugend hackt MV | Schwerin"
tags: jugendhackt hackathon
author: Felix
image: /assets/blog/jugendhackt-bild.jpg 
---
[![Jugend-Hackt-MV - CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)](https://farm2.staticflickr.com/1735/42032967554_87db09e0a9_z.jpg)](https://flic.kr/p/273iWR9 "CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)")

Anfang 2017 kam bei uns das erste Mal das Thema auf, ein eigenes Jugend hackt  zuveranstalten. Zusammen mit Andreas Beck vom [Landesjugendring](http://ljrmv.de/ljrmv/) wollten wir am liebstens sofort durchstarten. Die erste Hürde war allerdings die Finanzierung und an der scheiterte es auch. Wir hofften auf Unterstützung vom Land. Doch leider war der Haushalt bereits beschlossen und wir mussten auf nächstes Jahr warten.

Andreas ließ nicht locker und kam Anfang November wieder auf uns zu. Diesmal mit guten Nachrichten. Er hatte es geschafft, dass wir für 2018 eine Unterstüzung erhalten. Sofort suchten wir einen Termin und legten los. 30 Teilnehmer\*innen und 15 Mentoren\*innen sollten es werden. Das Hacklabor sollte die Teilnehmer\*innen und Mentoren\*innen suchen und der Landesjugendring wollte sich um das Organisatorische kümmern. Gesagt, getan.

Unsere anfänglichen Ängste nicht genügend Jugendliche zu finden, verflogen schnell. Immer mehr meldeten sich an und wollten teilnehmen. Nebenbei organsierte Andreas das Technologie und Gewerbezentrum Schwerin als Standort unseres Jugend hackt. Die Räumlichkeiten waren geklärt, nun mussten Übernachtungen her. Auch hier hat das Team vom LJRMV schnell eine Antwort gefunden und unsere Teilnehmer\*innen wurden im [Feriendorf Mueß](http://www.feriendorf-muess.de/) untergebracht.

An dieser Stelle wollen wir das erste Mal Danke sagen. Danke Andreas, danke an das Team vom Landesjugendring für die tatkräftige Unterstützung.

Wir mussten leider schnell feststellen, dass unsere Räumlichkeiten zu klein waren für die Jugendlichen und Mentoren\*innen. Bei einem ersten, kleinen Treffen der Mentoren\*innen in der [Frieda23](http://karo.ag/index.php/das-haus/) Rostock äußerten wir die Bedenken. Schnell kam Christoph auf uns zu und bot seine Unterstützung im Zusammenhang mit seinem Arbeitgeber an. [Trebing + Himstedt](https://www.t-h.de/), auch ansäßig auf dem TGZ Schwerin Gelände, stellten uns ihr gesamtes Gebäude für das Wochenende zur Verfügung. Dort hatten wir genügend Platz für alle und mehr. Es waren große Konferenzräume und Büroräume, jede Menge fahrbahre Flipcharts standen bereit, gemütliche Sitzsäcke für zwischen durch waren vorhanden, viele Wände waren magnetisch, so dass Notizen angepinnt werden konnten, überall WLAN und ein Kicker-Tisch im Foyer.

Wir möchten uns recht herzlich bei Trebing + Himsted bedanken und vor allem bei Christoph, der diese Zusammenarbeit erst ermöglicht hat.

## Tag 1

[![Jugend-Hackt-MV - CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)](https://farm2.staticflickr.com/1724/42751332051_5fde58d1a5_z.jpg)](https://flic.kr/p/288MKzV "CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)")

Nun aber zum Eigentlichen. __Jugend hackt MV | Schwerin__.
Am Kindertag, dem 1. Juni, 2018 war es soweit. 30 Teilnehmer\*innen und 16 Mentoren\*innen haben sich im TGZ Schwerin eingefunden und waren voller Erwartungen an das Wochenende. Es wurden die [Namen-Badges](https://twitter.com/hacklabor/status/1001982460454932484) verteilt (großen Dank an unseren anderen Christoph für den besonderen Einsatz diese rechtzeitig fertig zu stellen). Bevor es losging wurde das Hacklabor freudig inspiziert und schon die ersten Freundschaften geschlossen. Nach dem ersten Kennelernen beim Massen-Schnick-Schnack-Schnuck ging es auch schnell los, mit dem Finden von Projekten in den Themenräumen. In Diskussionen um Netzpolitik & Überwachung, Umwelt & Nachhaltigkeit, Bildung & Schule oder Freizeit, Gesundheit & Sport fanden sich die Jungendlichen zusammen und überlegten sich mögliche Projekte. Die Ideen waren breit gefächert und für jeden war etwas dabei.

Am Abend luden Micha und Murdoc noch zu einer Diskussionsrunde über die [Hacker](https://www.ccc.de/hackerethics)[ethik](https://tante.cc/2011/06/01/eine-neue-hackerethik/) ein. Man merkte schnell, dass die Teilnehmer\*innen nur Gutes im Sinn hatten und genau richtig bei Jugend hackt sind. Schließlich geht es bei Jugend hackt darum, mit Code die Welt zu verbessern.

## Tag 2

[![Jugend-Hackt-MV](https://farm2.staticflickr.com/1737/42750974591_1dea8c472c_z.jpg)](https://flic.kr/p/288KVjP)

Am Samstag fanden sich die Teilnehmer\*innen über die Projekte zu Gruppen zusammen. Es ging gemeinsam rüber zu den Räumen bei Trebing + Himstedt und alle legten freudig los mit dem Hacken. Zusammen mit den Mentoren\*innen wurde aus den Projekten auf Papier Code in der IDE. Der gesamte Tag wurde damit verbracht, die Idee in die Wirklichkeit umzusetzen.

Am Nachmittag gab es noch etwas Rahmenprogramm. In den Lightning Talks ging es um eine Einführung in Git, die Parlamentsdaten API [OParl](https://oparl.org/), IoT & Digitalisierung, reveal.md & hack.md, Jugend hackt Labs, Processing sowie um Wetterprognosen. Davor gab es eine Besichtigung des Rechenzentrums von [Planet IC](https://www.planet-ic.de/). Die Jugendlichen konnten so Neues lernen und ihren Wissenstand erweitern. 

## Tag 3

[![Jugend-Hackt-MV - CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)](https://farm1.staticflickr.com/880/41850329195_0285f76169_z.jpg)](https://flic.kr/p/26LaSPV "CC-BY 4.0 Jugend hackt, Foto: Jakob Waak (jacob-waak.de)")

Am Sonntag war es denn soweit. Noch schnell die letzten Zeilen Code schreiben und die Präsentation vorbereiten. Um 12.30 Uhr legten Micha und Andreas los und luden zur Vorstellung der Projekte ein. Zusammen mit den Teilnehmer\*innen und Mentoren\*innen sowie Eltern, Landtagsabgeordneten, Gästen  und den Teilnehmer des Livestreams ([YouTube](https://www.youtube.com/watch?v=dEkYt-gKBy4)) stellte jede Gruppe ihr Projekt vor. Aus einer Idee im Kopf ist nun etwas Handfestes entstanden. Man wollte mit Dronen und einem neuronalem Netzwerk den Müll bekämpfen, das langweilige Leben mit Golfspielen am Selfstick verschönern oder zusammen mit Amazon Alexa die ruhigsten Orte in seiner Umgebung finden.

Es war einfach super zu sehen, dass sich die Jugendlichen Gedanken um ihre Umwelt machten und an Ideen zu deren Schutz gearbeitet haben.

Eine Übersicht aller Projekte kann man bei [Hackdash](https://hackdash.org/dashboards/jhmv2018) finden.

Nach den Präsentationen war es leider auch schon vorbei. Noch schnell ein Gruppenfoto, Kontaktdaten ausgetauscht und dann ging es wieder nach Hause für alle.

Alle Fotos vom Wochenden gibt es bei [Flickr](https://www.flickr.com/photos/okfde/sets/72157696210849731/) zu sehen.

## Unser Dank an euch

Wir bedanken uns bei allen, die uns so tatkräfig oder finanziell unterstützt haben und hoffen, dass ihr auch beim nächsten Mal wieder mit dabei seid.

Ein besonderer Dank geht an die, die dieses Projekt erst ins Leben gerufen haben. Seit 2013 gibt es Jugend hackt und es wächst immer mehr. Wir bedanken uns beim Team aus Berlin und besonders bei Robert, Paula und Daniel.

Unser größter Dank geht an unsere Mentoren\*innen. Ohne sie hätten wir das alles nicht schaffen können. 
Danke an Andreas, Antje, Bastian, Ben, Christian, Christoph, Christoph, Claudia, Daniel, Felix, Friedhelm, Hanno, Jan, Jakob, Jasle, Johannes, Johannes, Lucas, Manfred, Matthias, Mehdi, Michael, Michael, Moe, Murdoc, Nadine, Paula, Paule, Reinhold, Robert, Ronald, Sebastian, Simeon, Stephan, Thomas.

Ein Dank auch an alle Partnerorganisationen:<br>
Landesjugendring Mecklenburg Vorpommern, Der Landesbeauftragte für Datenschutz und Informationsfreiheit Mecklenburg-Vorpommern<br>
Fördermittel:<br>
Das Land Mecklenburg Vorpommern<br>
Spenden:<br>
ATI Westmecklenburg, Codia, Com In, Leukhardt Schaltanlagen GmbH, Mandarin Medien, Trebing + Himstedt, unikat media

Unser Fazit zum Jugend hackt MV: Cool, wann ist das Nächste\*?
<br><br><br><br><br><br><br><br><br><br><br><br><br>
\* Vorraussichtlich 2019-06-14/16 Rostock-Warnemünde ;\)
