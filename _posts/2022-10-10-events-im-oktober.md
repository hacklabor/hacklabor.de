---
title: Events im Oktober
tags: events gaming workshop jugendhackt nachtdeswissens
author: Sebastian
---

# Kurzüberblick anstehender Events

Für den Oktober stehen mehrere Events an. 

Hier bekommt ihr einen kurzen Überblick: 

- 14.10.2022 19 Uhr [Gaming Friday mit Mario Kart Turnier im Tisch](https://hacklabor.de/2022/09/mario-kart-turnier-tisch/)
- 15.10.2022 14 Uhr [Eröffnung des Jugend hackt Lab Schwerin mit einem Workshop](https://hacklabor.de/2022/09/Eroeffnung-Jugend-hackt-Lab/)
- 22.10.2022 17-22 Uhr [Nacht des Wissens](https://www.nachtdeswissens-schwerin.de/)


