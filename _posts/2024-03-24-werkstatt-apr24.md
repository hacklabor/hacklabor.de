---
title: Offener Werkstatt Termin im April
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---
Du hast eine oder mehrere coole Projektideen, aber dir fehlt das passende Werkzeug oder du willst etwas selber reparieren? Dann komm doch am 6. Aoril 2024 zwischen 12 und 18 Uhr zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stehen bei Bedarf mit Tipps und Ratschlägen zur Seite.

#### Wann?
06.04.2024 - von 12 bis 18 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.
