---
title: Kochen im Hacklabor
tags: workshop public cooking
author: Sebastian
---
# Aller guten Dinge sind drei!
## Freitag den 05.03.2021, Start 20.00Uhr

Unser Kochstream am Freitag geht in die dritte Runde.
Diesmal wollen wir mit euch zusammen eine Erbsensuppe kochen.

Ihr könnt live auf [Twitch](https://www.twitch.tv/hacklabor) dabei sein und mitkochen.

Falls ihr mitmachen wollt, hier die Zutaten:

- 1kg grüne TK Erbsen oder im Glas
- 500g durchwachsenen Speck
- 1kg Kassler (als Kassler Kamm oder als Schälrippchen)
- 3 große Möhren oder 1x Suppengrün
- 3 mittelgroße Kartoffeln
- Pfeffer und Salz

Unser Koch: Th0m4s

Ihr könnt natürlich auch andere Varianten kochen. - Lasst uns am Ergebnis teilhaben und postet ein Bild auf Twitter mit dem Hashtag #kochenmitdemhacklabor

Danach seid ihr herzlich in die [Hacklabor World](http://world.hacklabor.de) eingeladen um über Dinge zu fachsimpeln oder einfach nur zum Quatschen.

**Kommt vorbei, wir freuen uns auf euch!**


### Diese Gerichte gab es schon:

- 24.02.2021 - [Soljanka](https://hacklabor.de/2021/02/kochen-soljanka/)
- 17.02.2021 - [Gulasch](https://hacklabor.de/2021/02/kochen-und-treffen/)