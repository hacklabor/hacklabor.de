---
title: Yes we're open
tags: eroeffnung geburtstag
author: micha
---

Nach mehreren Wochen Vorbereitung wurde am 24. September pünktlich um 13:37 Uhr das Hacklabor in Schwerin mit dem Durchschneiden des Cyberbandes eröffnet. In seiner Rede bedankte sich der Vorstand bei den Vereinsmitgliedern und Unterstützern<sup>\*</sup>, die das Hacklabor ermöglicht haben. 

![Vorstand schneidet Cyberband durch](/assets/blog/201609-yesweareopen/opening1.jpg)

Nach dem formellen Teil nutzten die Anwesenden umgehend die vorbereiteten Angebote. Beim Lötkurs gab es bis in den späten Nachmittag hinein keine freien Plätze. Die Einführung ins Lockpicking und der TicTacToe-Roboter erfreuten sich ebenfalls großer Beliebtheit. Wer nicht vor Ort war, konnte die Spiele auch im Internet verfolgen.

![TicTacToe-Roboter](/assets/blog/201609-yesweareopen/opening9.jpg)

![Lötkurs](/assets/blog/201609-yesweareopen/s-opening3.jpg)
![Hal Kuchen](/assets/blog/201609-yesweareopen/s-opening12.jpg)

![Im Hacklabor](/assets/blog/201609-yesweareopen/s-opening4.jpg)
![Lockpicking](/assets/blog/201609-yesweareopen/s-opening2.jpg)

Nicht nur Schweriner fanden den Weg zum Hacklabor. So durften wir auch Besucher aus dem Raum Salzwedel sowie aus Hamburg und Lübeck begrüßen. Die weiteste Anreise zur Eröffnung hatte aber ein Freifunker aus Chemnitz.

Besonders gefreut haben wir uns über die zahlreichen kleinen und großen Geschenke. Stellvertretend seien hier der Matekorb vom [Lübecker Chaotikum](https://chaotikum.org/news:hacklabinschwerineroeffnet), die Bitcoins von [unikat media](http://www.unikatmedia.de/) und unsere bisher einzige Pflanze vom [Coworking Schwerin](http://www.coworking-sn.de/) genannt. 

![Vorstand mit Bitcoins von unikat media](/assets/blog/201609-yesweareopen/s-opening7.jpg)
![Chuck vom Coworking Schwerin](/assets/blog/201609-yesweareopen/s-opening11.jpg)

Damit es Chuck, so wurde die Pflanze getauft, an nichts fehlt, wurden sofort allerhand Sensoren geplant.

Auch aus dem Internet gab es zahlreiche Glückwünsche zur Eröffnung.

<blockquote class="twitter-tweet" data-lang="de"><p lang="de" dir="ltr">Das <a href="https://twitter.com/hacklabor">@hacklabor</a> wurde eröffnet, wir waren da, es war super. Schweriner: Geht da hin! <a href="https://t.co/HIuMM7oL4p">https://t.co/HIuMM7oL4p</a> <a href="https://t.co/KqejoJ6Dys">pic.twitter.com/KqejoJ6Dys</a></p>&mdash; Chaotikum e.V. (@Chaotikum_eV) <a href="https://twitter.com/Chaotikum_eV/status/780087421564223488">25. September 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-conversation="none" data-lang="de"><p lang="de" dir="ltr"><a href="https://twitter.com/hacklabor">@hacklabor</a> viel Spaß und Erfolg aus Münster :3</p>&mdash; warpzone.ms (@warpzone_ms) <a href="https://twitter.com/warpzone_ms/status/779709401947074560">24. September 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-conversation="none" data-lang="de"><p lang="de" dir="ltr"><a href="https://twitter.com/hacklabor">@hacklabor</a> viele Glückwünsche auch aus Erfurt!</p>&mdash; Bytespeicher Erfurt (@bytespeicher_ef) <a href="https://twitter.com/bytespeicher_ef/status/779665675459649537">24. September 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="de"><p lang="de" dir="ltr"><a href="https://twitter.com/hacklabor">@hacklabor</a> Alles Gute zur Eröffnung wünscht der Neubrandenburger Hackerspace ;)</p>&mdash; Entität (@Entitaet) <a href="https://twitter.com/Entitaet/status/779661562470604800">24. September 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Ein Highlight mit hohem Spaßfaktor war das Hacker-Jeopardy. Bei den, über den Tag verteilten 3 Runden ging es hoch her. Die lauteste Publikumsreaktion rief die leider falsche Frage auf die Antwort "Steve Jobs" hervor.

![alt text](/assets/blog/201609-yesweareopen/s-opening8.jpg)
![alt text](/assets/blog/201609-yesweareopen/s-opening10.jpg)

Vor dem Hacklabor konnte man sich mit Hilfe von [Siebfreak und Roy](https://www.facebook.com/siebfreakundroy/) unter anderem unser Logo auf sein T-Shirt drucken. Ein weiteres beliebtes Motiv war der Schrotthering, der seinen Weg auf etliche Klamotten fand.  

![Siebdruck](/assets/blog/201609-yesweareopen/s-opening6.jpg)
![Bedruckte T-Shirts](/assets/blog/201609-yesweareopen/s-opening5.jpg)

Wer mit seinem Besuch nicht bis zum nächsten großen Event warten möchte, schaut Dienstag oder Freitag ab 19 Uhr bei uns im TGZ rein.

<sup>\*</sup>Unterstützer: [TGZ](http://tgz-mv.de), [Planet IC](http://planet-ic.de), [Kirchgemeinde Gadebusch](http://www.kirche-gadebusch.de/), 
[IT Point MV](http://www.it-point-mv.de/), [Holger Hempel](http://www.kfs-hempel.de), [LVG mbH Schwerin &  Co. KG](https://lvg-schwerin.de/), ...
