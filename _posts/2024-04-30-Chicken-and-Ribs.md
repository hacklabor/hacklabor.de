---
title: Chicken and Ribs
tags: event
author: Thomas
---
# Chicken and Ribs
![](/assets/blog/2024-05-02-ChickenandRibs.jpg)


#### Wann?
02.05.2024 - um 19 Uhr bis 21 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung wie immer kostenlos. Spenden für den Verein sind erwünscht.


