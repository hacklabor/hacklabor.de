---
title: Gesellschaftsspiele Abend im Februar
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Gesellschaftsspiele Abend im Hacklabor

Der Januar ist zur Hälfte geschafft und wir möchten wieder mit euch Gesellschaftsspiele spielen. Also kommt am Samstag den 8. Februar Abends ab 18 Uhr in das Hacklabor. 
Es sind zwar reichlich Spiele vorhanden aber wie immer freuen wir uns wenn ihr euer Lieblingsspiel mitbringt.

Die Veranstaltung ist wie immer kostenfrei.

Was?  - Gesellschaftsspiele Abend im Hacklabor  
Wann? - Samstag 08.02.2020 ab 18 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
PS: [So kommst du zu uns](/standort/)