---
title: How to not get Bundestaghacked™
tags: workshop, crypto, veranstaltung
author: Micha, Murdoc
image: /assets/blog/hacker.jpg
---

## Update - Präsentation
Zum nachlesen haben wir hier jetzt unsere Präsentation dazu veröffentlicht: [Bundestaghacked.pdf](/assets/blog/Bundestaghacked.pdf)

## Workshop
Wieso war der Bundestaghack™ kein Hack und wie kann man sein Online-Leben besser absichern?

![Hacker](/assets/blog/hacker.jpg "Hacker")

Dazu laden wir euch zu einem Workshop mit folgenden Themen ein:

* Sicherheit von Passwörtern
* Passwörter verwalten
* Umgang mit den eigenen Daten im Internet
* sichere Kommunikationswege

Wir haben Zeit mitgebracht und wollen euch mit der vorgestellten Software nicht allein lassen. Darum bitten wir euch, eure Laptops und Smartphones mitzubringen, damit wir euch bei der Einrichtung helfen können. 

## Wann?

Mittwoch 2019-01-30 ab 19:00

## Wo?

[Im Hacklabor](https://hacklabor.de/standort/)

## Voraussetzung

* Laptop
* Telefon
* Interesse

## Kosten

Die Teilnahme am Workshop ist kostenlos. Getränke können mitgebracht oder vor Ort erworben werden.