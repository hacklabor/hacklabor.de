---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Weihnachtlicher Workshop
## Samstag den 17.12.2022, 14 bis 18 Uhr

Es wird weihnachtlich im Schweriner Jugend hackt Lab! 🎅

Design und große Technik stehen in diesem Workshop gleichermaßen im Mittelpunkt. Zunächst wollen wir gemeinsam mit dir das freie Vektorgrafikprogramm Inkscape kennenlernen und ein paar weihnachtliche Motive gestalten. Danach geht es direkt an den Lasercutter, um die am Computer designten Objekte in die echte Welt zu bringen.

Du benötigst dafür keine Vorkenntnisse. Egal ob dekorativer Weihnachtsstern für den heimischen Weihnachtsbaum oder einzigartige Geschenkbox – deiner Kreativität sind keine Grenzen gesetzt und du wirst mit deinem eigenem, selbst gestaltetem Werkstück aus Holz nach Hause gehen. 🎄

Die ideale Gelegenheit um kurz vor Weihnachten noch ein kreatives Geschenk selbst zu basteln. 🎁

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2022-12-17/)

Wir freuen uns auf dich! 🦙

#### Wann?
17.12.2022 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

