---
title: event
tags: events, basteln, öffentlich
author: Marko, Thomas
---
# Adventsbasteln 2019
## Samstag den 30.11.2019, Beginn 15.00 Uhr 
![Adventsbasteln2019](/assets/blog/Adventsbasteln2019.png)

Wie jedes Jahr nähert sich Weihnachen mit großen Schritten. Alljährlich steht die Frage im Raum: Was soll ich Schenken? Nicht zu teuer und am besten mit Liebe selbstgemacht. Deswegen möchten wir euch Einladen, um mit uns:

- einen kleinen LED Tannenbaum zu löten
- Adventschmuck aus dem Laser zusammenzubauen
- ein LED Windlicht zu basteln

Für das leibliche Wohl ist gesorgt bei Plätzchen, frischen Waffeln, selbst gemachten Kuchen und Kaffee.

Am Abend werden wir noch die Feuerschale anmachen (vorbehaltlich Wetter) und den Tag bei Kinderpunsch und Glühwein ausklingen lassen.


Die Veranstaltung ist kostenfrei. Bringt aber ggf. etwas Kleingeld für Getränke und dem Umkostenbeitrag fürs Material (<5 Euro) mit.

PS: [So kommt ihr zu uns](/standort/)




