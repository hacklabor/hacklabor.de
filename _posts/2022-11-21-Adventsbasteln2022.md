---
title: Weihnachten im Hacklabor
tags: events, basteln, öffentlich, Advent
author: Gerd
---
# Adventsbasteln 2022
## Samstag den 03.12.2022, Beginn 15.00 Uhr 
![Adventsbasteln2022](/assets/blog/Adventsbasteln2019.png)

Wie jedes Jahr nähert sich Weihnachten mit großen Schritten. Alljährlich steht die Frage im Raum: Was soll ich schenken? Nicht zu teuer und am besten mit Liebe selbst gemacht. Deswegen möchten wir euch einladen, um mit uns verschiedene weihnachtliche Lötkits (LED Tannenbaum, Sterne, Herzen und mehr) sowie Weihnachtsdeko aus dem Lasercutter zu basteln.

Für das leibliche Wohl ist mit Plätzchen, frischen Waffeln, selbst gebackenem Kuchen und Kaffee gesorgt.

Am Abend werden wir noch die Feuerschale anmachen (vorbehaltlich Wetter) und den Tag bei Kinderpunsch und Glühwein ausklingen lassen.

Wir freuen uns auf euren Besuch!

#### Wann?
03.12.2022 - ab 15 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

PS: [So kommt ihr zu uns](/standort/)

#### Eintritt
Die Veranstaltung ist wie immer kostenlos. Bringt aber, wenn ihr möchtet, etwas Kleingeld für Getränke und Material mit.









