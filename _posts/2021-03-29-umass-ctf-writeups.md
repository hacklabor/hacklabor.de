---
title: CTF Time
tags: ctf writeup cybercyber 
author: hacklabor
---
# UMassCTF 21 Writeups und Videos

Wir haben am Wochenende beim [UMassCTF 2021](https://ctf.umasscybersec.org/) mitgemacht.
[Hier](https://ctf.umasscybersec.org/teams/178) gehts zu unserem Team Ergebnis.

Für zwei Challenges haben wir jeweils ein Writeup und ein Video veröffentlicht.

Challenge: scan_me 
- Category: MISC
- [Writeup](https://gitlab.com/hacklabor/ctf/writeup/-/blob/master/misc/scan_me_umassCTF21/writeup.md)
- [Video](https://youtu.be/vM9CNvdbyKw)

Challenge: Easteregg
- Category: reverse
- [Writeup](https://gitlab.com/hacklabor/ctf/writeup/-/blob/master/reverse/easteregg_umass21/easteregg_.md)
- [Video](https://youtu.be/l9zRBJJYTgg)



 