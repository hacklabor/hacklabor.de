---
title: Programmiere deine eigene Drum Machine
tags: workshop
author: micha
image: /assets/blog/2018-meetcode-drums-min.png
---

![Programmiere deine eigene Drum Machine](/assets/blog/2018-meetcode-drums-min.png)

Im Rahmen des Workshop programmierst du nicht nur deine eigene Drum Machine, du lernst auch noch die Grundlagen der modernen Webentwicklung. Wir werden zusammen eine Webanwendung erstellen und dabei HTML, CSS und JavaScript nutzen.

Ziel des Workshops ist es das Interesse am Programmieren zu wecken und zu zeigen wie einfach der Einstieg in die Webentwicklung ist.

Der Workshop besteht aus kurzen Vortragspassagen zur Erklärung der Theorie gefolgt von kurzen Demos der gezeigten Konzepte. Den Hauptteil stellt jedoch die Arbeit der Teilnehmer an ihrem Projekt dar.

Diese Veranstaltung richtet sich an Einsteiger von 16 -24 Jahren.

## Termin
13 Oktober 2018 von 14:00 - 18:00 Uhr

## Anmeldung & Eintritt
Die Teilnehmerzahl ist auf 20 begrenzt. Eine [Anmeldung](https://goo.gl/forms/mY1yrCU7gl1QZ7Es2) ist erforderlich. Die Teilnahme ist kostenlos.

## Wo?
Hacklabor Schwerin, Hagenower Straße 73, 19061 Schwerin

Wir freuen uns über zahlreiches Erscheinen.

PS: [So kommst du zu uns](https://hacklabor.de/standort/) 