---
title: Weihnachten im Hacklabor
tags: events, basteln, öffentlich, Advent
author: Christoph
---
# Adventsbasteln 2023
## Samstag den 09.12.2023, Beginn 14.00 Uhr


Weihnachten kommt mit großen Schritten und wir möchten euch einladen mit uns verschiedene weihnachtliche Lötkits (LED Tannenbaum, Sterne, Herzen und mehr) sowie Weihnachtsdeko aus dem Lasercutter zu basteln. Diese eignen sich perfekt, um sie zu verschenken oder es sich zu Hause etwas heimeliger zu machen.

Für das leibliche Wohl ist mit Plätzchen, selbst gebackenem Kuchen und Kaffee gesorgt.

Am Abend werden wir den Tag mit Kinderpunsch und Glühwein ausklingen lassen.

Wir freuen uns auf euren Besuch!

#### Wann?
09.12.2023 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

PS: [So kommt ihr zu uns](/standort/)

#### Eintritt
Die Veranstaltung ist wie immer kostenlos. Bringt aber, wenn ihr möchtet, etwas Kleingeld für Getränke und Material mit.
