---
title: Hacking time
tags: event ctf hacking workshop cyber-security
author: Sebastian
---

# CTF Workshop für Beginner und Fortgeschrittene
## Samstag den 22.04.2023, ab 12 Uhr

Wir wollen zusammen eure und unsere Skills beim [ångstromCTF 2023](https://angstromctf.com/) aufbauen und erweitern. Für CTF (”Capture the flag”) Einsteiger wird es Erklärungen und Einführungen in Hacking Techniken geben.

Bei diesem CTF-Wettbewerb werden wir vor eine Reihe von Herausforderungen gestellt, die darauf abzielen, bestimmte digitale "Flaggen" zu finden, die normalerweise in verschiedenen Aufgaben oder Systemen versteckt sind.

Gemeinsam bauen wir das Wissen in verschiedenen Bereichen wie Netzwerksicherheit, Kryptografie, Reverse Engineering, Webanwendungen und OSINT auf, um die Flags zu finden und zu lösen.

CTF Aufgaben sind nicht nur für Fachleute oder Experten in Cybersecurity. Es gibt auch Einsteiger-Level-Aufgaben. Diese Einsteiger-Level-Herausforderungen sind in der Regel weniger komplex und unkomplizierter, was uns die Möglichkeit gibt, die Grundlagen zu lernen, bevor wir zu fortschrittlicheren Herausforderungen übergehen.

Im letzten Jahr haben wir beim ångstromCTF 2022 unser bestes Ergebnis in 2022 mit [12,508 CTF Punkten](https://ctftime.org/event/1588) erreicht.
Das wollen wir nach Möglichkeiten gerne wieder erreichen oder verbessern.

Kommt vorbei!

#### Wann?
22.04.2023 - ab 12 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.

### Links
- [ångstromCTF 2023](https://angstromctf.com/)
- [CTF Time](https://ctftime.org/event/1859)

