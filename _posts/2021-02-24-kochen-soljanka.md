---
title: Soljanka kochen
tags: workshop world public cooking 
author: Sebastian
---
# Wir kochen mit euch Soljanka
## Freitag den 26.02.2021, Start 20.00Uhr

Diesmal wollen wir mit euch zusammen eine Soljanka kochen.

Ihr könnt wieder live auf [Twitch](https://www.twitch.tv/hacklabor) dabei sein und mitkochen.

Hier die Zutaten:

- 1 Glas Letscho
- 1 Glas Gurkensticks Süß-Sauer
- 1 Packung passierte Tomaten ca 500g
- ca. 500g Jagdwurst am Stück
- ca. 500g Salami am Stück
- 4 Stk. Zwiebeln
- 5 Knoblauchzehen
- 1 Becher Schmand oder saure Sahne (wer mag)
- Pfeffer
- Salz
- Paprika
- Zucker
- Essig

Ihr könnt natürlich auch andere Varianten kochen. - Lasst uns am Ergebnis teilhaben und postet ein Bild auf Twitter mit dem Hashtag #kochenmitdemhacklabor

Danach seid ihr herzlich in die [Hacklabor World](http://world.hacklabor.de) eingeladen um über Dinge zu fachsimpeln oder einfach nur zum Quatschen.

![Soljanka im Hacklabor](/assets/blog/2021-02-24-kochen-soljanka/soljanka-hacklabor.jpg)

**Kommt vorbei, wir freuen uns auf euch!**
