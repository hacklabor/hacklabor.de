---
title: OpenBikeSensor im Hacklabor
tags: ESP Fahrrad 
author: Christoph
---
# OpenBikeSensor im Hacklabor 

der ein oder andere hat es vielleicht schon mitbekommen – vom Zweirat Stuttgart gibt es das OpenBikeSensor Projekt. Dabei handelt es sich um ein kleines technisches Gerät am Fahrrad, das während der Fahrt den Abstand von überholenden Autos misst.
Rund um das Projekt gibt es mittlerweile viele Leute die sich aktiv an dem Projekt beteiligen. Sei es mit Hardware wissen, Visualisierung auf Karten und vielem mehr.
So werden auch in vielen Städten Workshops angeboten, bei denen sich Leute Ihren eigenen Sensor bauen können. Genau hier möchte das Hacklabor mitmachen und den im Großraum Schwerin beheimateten Radlerinnen die Möglichkeit bieten ihren Sensor gegen einen Unkostenbeitrag zu bauen. Termine hierfür gibt es demnächst an dieser Stelle.

weitere Infos unter: [https://zweirat-stuttgart.de/projekte/openbikesensor/](https://zweirat-stuttgart.de/projekte/openbikesensor/)
