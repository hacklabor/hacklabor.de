---
title: Nacht des Wissens Schwerin
tags: veranstaltung vortrag mitmachen
author: Sebastian
---
# Vorträge und Mitmachen auf der Nacht des Wissens 2024 
## Samstag den 19.10.2024, 17 bis 22 Uhr

Dieses Jahr findet die 9. Nacht des Wissens in Schwerin statt.
Auch das Hacklabor ist wieder mit dabei und  bietet ein spannendes Programm aus Vorträgen und Mitmachaktionen.

Ab 17 Uhr könnt ihr bei unserem spannenden Programm teilnehmen.  
**Der Eintritt ist kostenfrei.**

**UNSER PROGRAMM**

**17 - 22 Uhr**

***Führung durch das Hacklabor***

***Löten für Kinder***   
In diesem Workshop lernst du löten. Deine bunt blinkende 
Kreation kannst du danach mit nach Hause nehmen.

***3D Drucker in Aktion***   
Schau dir unseren 3D Drucker in Aktion an und lass dir erklären, wie er funktioniert.


**VORTRÄGE**

18:00 Uhr  
**HACKER, HACKERSPACES UND DIE OFFENE WERKSTATT - WAS IST DAS EIGENTLICH?**  
Eine Einführung in die Welt der Hackerspaces, der Hackerkultur und die offene Werkstatt im Hacklabor. Außerdem gibt es eine virtuelle Führung durch die Werkstatt und eine Erklärung, wie du sie nutzen kannst. 

*Dauer 45 Minuten*
 
19:00 Uhr  
**FAKES, FAKTEN UND DIE FOLGEN: WIE KÜNSTLICHE INTELLIGENZ UNSERE REALITÄT VERZERREN KANN**  
Fake News sind wahrhaftig kein neues Phänomen, doch Künstliche Intelligenz erlaubt bisher 
ungeahnte Möglichkeiten großangelegter Desinformation. Von täuschend echten Fotomontagen über 
computergenerierte Stimmimitationen bis hin zu gefälschten Videoaufnahmen - die Fähigkeit 
von K.I., Fakes zu erzeugen, scheint grenzenlos. Doch während diese Technologien unsere Welt 
verändern, bleiben viele Fragen offen. Was bedeutet es, wenn wir nicht mehr sicher sein 
können, ob das, was wir sehen und hören, echt ist? Wie beeinflussen K.I.-generierte Fakes 
unsere Entscheidungen und unser Vertrauen in Informationen? In diesem Vortrag werfen wir einen 
Blick auf die gesellschaftlichen Auswirkungen der K.I.-Revolution und diskutieren, wie wir 
mit den damit verbundenen Herausforderungen umgehen können.  

*Dauer 45 Minuten*

20:00 Uhr  
**JEDES BILD ERZÄHLT EINE GESCHICHTE: DIE VERBORGENE WELT UNSERER ONLINE-IDENTITÄT**  
Taucht mit uns in die Welt von Open Source Intelligence (OSINT) ein, indem wir die 
verborgenen Informationen in den Fotos ausfindig machen, die wir online teilen. 
Entdeckt, wie jedes Bild subtile Details über unsere Identität, Gewohnheiten und 
Standorte enthüllt. Wir zeigen Techniken, wie diese Daten extrahiert werden und 
diskutieren die Auswirkungen auf unsere Privatsphäre und Sicherheit. Ein Blick hinter 
die Kulissen unserer digitalen Spuren.  

*Dauer 45 Minuten*

**INFOS ZUR NACHT DES WISSENS**  
Weitere Infos zur Veranstaltung und den teilnehmenden Einrichtungen findet ihr hier: [https://www.nacht-des-wissens-schwerin.de](https://www.nacht-des-wissens-schwerin.de)


**Kommt vorbei, wir freuen uns auf euch!**
