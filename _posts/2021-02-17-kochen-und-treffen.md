---
title: Treffen und kochen
tags: workshop world public cooking 
author: Sebastian
---
# Wir treffen uns digital und kochen ein Gulasch
## Freitag den 19.02.2021, Start 20.00Uhr

In alter Tradition wollen wir an unserem Treffen am Freitag den Kochlöffel schwingen.
Vielleicht wird dabei auch die allseits diskutierte Frage: "Fleisch oder Zwiebeln zuerst" geklärt?

Neben dem öffentlichen Treffen in der digitalen Welt [world.hacklabor.de](http://world.hacklabor.de) wird parallel auf [Twitch](https://www.twitch.tv/hacklabor) ein Koch-Workshop stattfinden.

Wir wollen mit euch ein Gulasch kochen.
Wer mitkochen möchte bekommt hier die Zutaten:

- 1kg Gulaschfleisch halb Schwein halb Rind
- 5 STK mittelgroße Zwiebeln
- 0.5l Buttermilch
- Paprikapulver
- Pfeffer
- Salz

    
Ihr könnt natürlich auch andere Varianten kochen. - Lasst uns am Ergebnis teilhaben und postet ein Bild auf Twitter mit dem Hashtag #kochenmitdemhacklabor

![Gulasch im Hacklabor](/assets/blog/2021-02-17-kochen-und-treffen/gulasch-hacklabor.jpg)

**Kommt vorbei, wir freuen uns auf euch!**
