---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Sebastian
---
# Open Lab
## Samstag den 19.11.2022, 14 bis 17 Uhr

Bei unserem zweiten Jugendhackt Lab Schwerin Termin ist das Thema "Open Lab".  

Wenn du Unterstützung beim Programmieren brauchst, löten, den 3D Drucker oder Lasercutter ausprobieren möchtest - komm vorbei! 

Und falls du noch keine eigene Idee hast, finden wir bestimmt gemeinsam ein spannendes Projekt für dich.

Deine Vorkenntnisse spielen dabei überhaupt keine Rolle und alle Jugendlichen zwischen 12 und 18 Jahren sind herzlich willkommen!

Eine Anmeldung ist nicht erforderlich. Komm einfach vorbei und lasst uns mit Code die Welt verbessern!

Wir freuen uns auf dich! 🦙

#### Wann?
19.11.2022 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

