---
title: "Einsteigerkurs - Arduino UNO"
tags: arduino uno electronic
author: moe
---

[![SomeCode](/assets/blog/20-05-2018_ET-Arduino_Vortrag.jpeg)](https://www.facebook.com/events/496041690812286)


Der Vortrag wurde recht kurfristig angesetzt, da Thomas und der Space gerade einen freien Termin haben. 
Alle die Arduino und Elektrotechnik Grundlagen interessiert sind koennen gerne vorbei kommen, auch die die nicht auf dem ersten Arduinovortrag waren. 

Und es gibt auch Kuchen :) 

#### Wann?
20.05.2018 - ab 15 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Vortragender 
Thomas

#### Anmeldung
keine - einfach vorbeikommen.

#### Eintritt
Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.

Wir freuen uns auf das zahlreiche Erscheinen der angehenden Makerinnen und Maker, Hacker und Haecksen ( Hackerinnen ). Auf bald bei uns im Hacklabor.

PS: [So kommst du zu uns](/standort/)