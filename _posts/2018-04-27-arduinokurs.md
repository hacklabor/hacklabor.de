---
title: "Einsteigerkurs - Arduino UNO"
tags: arduino basics
author: moe
---

[![SomeCode](/assets/blog/2018-arduinokurs.jpeg)](https://www.facebook.com/events/194495824683063)


Sind Hardware-Basteleien und Mikrokontroller für Euch Neuland? - Verzweifelt nicht! Manfred kann und will Euch helfen.

Am Samstag, den 12. Mai, dreht sich im Hacklabor alles um den Open Source Mikrocontroller Arduino und Fragen wie "Wie schließe ich LEDs an?" und "Wann fangen die endlich an zu blinken?"

Zunächst werden in einem kurzen Vortrag die Arduino-Hardware selbst sowie die Grundlagen der Hard- und Software-Entwicklung für die Plattform vorgestellt. Der Großteil der Zeit ist jedoch dem "Spaß am Gerät" vorbehalten. Ihr könnt selbst aktiv werden und Eure ersten Experimente mit dem Arduino UNO starten. Hier erfahrt Ihr, wie einen Arduino UNO programmiert und in einfache Schaltungen eingebunden wird. Am Ende werdet Ihr bestimmt alle blinken … oder zumindest die diversen LEDs an Euren Arduinos.


Wenn Ihr Technik und Bastelmaterial habt, bringt es mit! Gerne gesehen sind insbesondere:

* Eurer eigener Arduino (UNO)
* Eurer Notebook 
* Steckbrett zum Aufbauen von Experimenten
* Jumperkabel (kann man nie genug haben)
* LEDs rot 5mm
* Widerstände 470 Ohm

[Komplettes UNO Projekt-Einsteiger-Set](https://www.amazon.de/dp/B01DGD2GAO/)


In begrenztem Umfang können wir auch mit Notebooks, einigen Arduinos und anderem Kleinmaterial aushelfen.



#### Wann?
12.05.2018 - ab 15 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Vortragender 

Manfred 


#### Anmeldung
Bitte meldet Euch vorab unter [inbox@hacklabor.de](mailto:inbox@hacklabor.de) an und teilt uns mit, ob und was Ihr mitbringen wollt. 
Dies hilft uns bei der Planung der Plätze und des notwendigen Materials. Aber keine Sorge - auch die Kurzentschlossenen kommen noch rein.



#### Eintritt
Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.



Wir freuen uns auf das zahlreiche Erscheinen der angehenden Makerinnen und Maker, Hacker und Haecksen ( Hackerinnen ). Auf bald bei uns im Hacklabor.

PS: [So kommst du zu uns](/standort/)