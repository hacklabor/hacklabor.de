---
title: カレーライス karē raisu Essen
tags: kochen japanisch hacklaborevent
author: ThomasK
---

![](https://upload.wikimedia.org/wikipedia/commons/8/82/Matsuya_Original_Curry_and_rice.jpg)
By Corpse Reviver (Own work) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY 3.0 (http://creativecommons.org/licenses/by/3.0)], via Wikimedia Commons

[https://de.wikipedia.org/wiki/Kar%C4%93](https://de.wikipedia.org/wiki/Kar%C4%93)

__Wer Lust hat auf Japanische Küche, kommt am Freitag um 19.00 Uhr zum Curry Reis Essen zu uns.__

- Zubereitung ab ca. 14:00 Uhr, Essen ab 19.00 Uhr

Neun Leute können auch in den Genuss eines ASAHI Super DRY Bieres kommen. Das schmeckt so wie es heißt und wir es von trockenen Wein her kennen. Last euch überraschen!

wer sich noch Informieren möchte hier das Grundrezept:

[https://www.chopstickchronicles.com/japanese-beef-curry-rice/](https://www.chopstickchronicles.com/japanese-beef-curry-rice/)

Auf Wunsch mach ich einen kleinen Topf auch ohne Beef ;). Meldet euch bei mir.


Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)


"kostenlosen Essen" gibt es nicht! Aber "Free Lunch"

und natürlich nur "solange der Vorrat reicht" TM


