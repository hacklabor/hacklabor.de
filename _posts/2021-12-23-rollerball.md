---
title: Wird das jetzt ein Flipper?
tags: rc3 project nowhere rollerball
author: Sebastian
---

# Von der Idee ein Spiel zu bauen

Im letzten Blogpost [Hacklabor im NOWHERE](https://hacklabor.de/2021/12/hacklabor-nowhere/) haben wir davon berichtet
dass wir wieder ein Spiel zur rC3 bauen wollen.  
Wieder physisch im Hacklabor, steuerbar über das Internet.

"Nur was soll es diesmal werden", stellten wir uns als Frage.

Es muss anders als [Virus Hunt](https://www.youtube.com/watch?v=hG8GM0TPLhM) werden. Obwohl das Thema leider immernoch präsent ist.  
Vielleicht hätten wir doch weiter auf Virus Jagd gehen sollen? ;)

Anders bedeutet auch immer eine Herausforderung.  
Bei der Ideensammlung kam der Vorschlag eines Mitgliedes, einen Flipper zu bauen.  
Alle Projektmitglieder waren vom Thema begeistert. Nach Überlegungen und beim Blick auf den Kalender mussten wir uns eingestehen, so wie wir uns das vorstellen ist das nicht bis zur rC3 zu schaffen.  
Davon ließen wir uns nicht aus der Bahn werfen. Weitere Ideen zur Umsetzung und zur Story wurden gesammelt, erste Zeichnungen und Konstruktionen wurden erstellt. 

Protypen zur Machbarkeit wurden gebaut und es kristallisierte sich immer mehr unser neues Spiel unter dem Arbeitstitel "Rollerball" heraus. Ganz vielleicht finden sich Elemente von einem Flipper wieder.  
Panels, LEDs und Sensoren mussten getestet werden. 
Bevor die Hardware zusammengebaut werden konnte, brauchten wir ein Layout für das Spielbrett.  
Wie sieht die Story aus? Wie können Punkte erlangt werden?  
Es war viel Planung und viele Absprachen notwendig damit später alles ineinander greift.

Nun leuchtet alles bunt und dann muss auch noch alles programmiert werden. 

Seid ihr auch schon so gespannt?

![rc3_project_04](/assets/blog/2021-12-23-rollerball/spielfeld_V0.2.jpg)

![rc3_project_05](/assets/blog/2021-12-23-rollerball/IMG_20211128_150305.jpg)

![rc3_project_06](/assets/blog/2021-12-23-rollerball/Slack-Bild-(2021-12-11_13-38-24).jpg)

