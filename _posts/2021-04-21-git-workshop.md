---
title: Online Workshop Git Grundlagen
tags: git source control vcs gitlab workshop public
author: Gerd
---

![git-workshop](/assets/blog/2021-04-21-git-workshop.png)

In diesem Einsteiger-Workshop geht es um die Vermittlung der absoluten Git-Grundlagen:
Was ist eigentlich ein Versionsverwaltungssystem und warum möchte ich sowas benutzen? Was ist Git und was Gitlab? Wie benutze ich das und wie kann ich es verwenden, um mit anderen Leuten gemeinsam an Projekten zu arbeiten?

Nach einer kurzen Einführung sollen Antworten auf diese und andere Fragen keineswegs durch reine Frontalbeschallung, sondern vielmehr hands-on durch praktische Übungen vermittelt werden.

Jeder Teilnehmer wird nach dem Workshop den grundlegenden Workflow bei der Arbeit mit Git kennen und in der Lage sein, eigene Projekte mit Git zu versionieren und auf Gitlab zu teilen sowie Repos anderer zu verwenden.

Das Ganze wird online über BigBlueButton stattfinden und voraussichtlich zwei Stunden in Anspruch nehmen.


#### Wann?
27.04.2021 - ab 19 Uhr

#### Wo?
Online - [HIER](https://bbb.hacklabor.de/b/ger-g17-eml-6yw)

#### Voraussetzungen
* Eigener PC mit Internetanbindung ^^
* Headset oder Mikro zur Kommunikation

#### Eintritt

Die Veranstaltung ist wie immer kostenlos.

Wir freuen uns auf Dich!

