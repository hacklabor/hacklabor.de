---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Anma, Gerd
---

# Workshop: Die Hedgebox - Ein smartes Igelhaus
![Workshop Hedgebox](/assets/blog/2024-10-24-Jugend-hackt-Workshop.jpg)

Du bist begeistert von Technik und Natur? Du arbeitest gern mit Holz und am Computer?

Dann bist Du hier genau richtig.

In diesem zweiteiligen Workshop bauen wir gemeinsam ein Winterquartier für Igel, das technisch so ausgestattet ist, dass Du von außen schauen kannst, ob sich ein kleiner stachliger Gast eingenistet hat, ohne seinen Schlaf zu stören. 🦔

Ob in der Stadt oder auf dem Dorf, ganz in unserer Nähe leben zahlreiche Wildtiere, die sich meist gut vor uns verstecken. Mit einem Holzhaus bietest Du einem Igel einen sicheren Platz für den Winterschlaf und vielleicht gefällt es ihm so gut, dass er das ganze Jahr zum Schlafen bei Dir einzieht.

In einem Workshop-Teil wird das Holzhaus zusammengebaut und wir schauen uns an, wie und wo Du es am besten aufstellen kannst. Während des Winterschlafs sollte der Igel nicht gestört werden – aber woher weißt Du dann, ob überhaupt einer im Haus ist? Dafür werden im zweiten Workshop-Teil Sensoren eingerichtet, mit denen Du von außen überprüfen kannst, was im Inneren passiert, ohne den kleinen Bewohner zu stören.

Egal, ob Du schon Erfahrung hast, oder Dich ganz neu mit den Themen beschäftigen willst, bei uns bist Du herzlich willkommen. Hier kannst Du Neues lernen, Deine Fähigkeiten ausprobieren und Dich mit Gleichgesinnten austauschen.

Wir freuen uns auf dich! 🦙


#### Wann?
26.10.2024 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

