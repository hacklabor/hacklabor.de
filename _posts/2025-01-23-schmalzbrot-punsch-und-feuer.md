---
title: Schnalzbrot und Punsch am Feuer
tags: event public outdoor
author: Sebastian
---
Lasst uns gemeinsam die Winterkälte vertreiben und einen geselligen Abend am Feuer genießen! 🔥✨

🍞 Frisches Brot und selbst gemachtes Apfel-Gänseschmalz. 

🌱 Natürlich gibt es auch eine vegetarische Alternative!

🍷 Für wohlige Wärme sorgen Punsch und Glühwein.

Kommt vorbei, bringt gute Laune mit und lasst uns zusammen den Winter feiern! ❄️

Wir freuen uns auf euch!

#### Wann?
25.01.2025 - 17 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.
