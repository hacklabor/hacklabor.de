---
title: Gesellschaftsspiele Nachmittag im Januar
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Gesellschaftsspiele Nachmittag im Hacklabor

Am 12.Januar ab 14 Uhr ist es wieder soweit. Kommt vorbei und spielt mit uns im warmen Hacklabor Gesellschaftsspiele während es draußen fröstelt und schneit. Für Mate, Kaffee und natürlich Kuchen ist wieder gesorgt.

Die Veranstaltung ist kostenfrei.

Was?  - Gesellschaftsspiele Nachmittag im Hacklabor  
Wann? - Sonntag 12.01.2020 ab 14 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
Gern könnt Ihr auch wieder euer Lieblingsspiel mitbringen.
        
PS: [So kommst du zu uns](/standort/)
