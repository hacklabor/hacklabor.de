---
title: "Linux Installation - Arch"
tags: linux arch install os
author: moe
image: /assets/blog/Archlinux-Workshop.png
---

Du willst dir ein Linux installieren. Gentoo findest du zu krass. Ubuntu macht nicht so richtig was du willst und eigentlich möchtest du dein System schon etwas besser im Griff haben.
[![ArchLinux-Workshop-MetalNerd](/assets/blog/Archlinux-Workshop.png)](https://www.facebook.com/events/275414479982662/)

Dafür ist dann Arch-Linux wie gemacht. Ein System in dem man dank des Arch Repository und AUR alles findet was das Herz begehrt und man nicht all zu viel von Hand kompilieren muss.

yuna erklärt wie man dies macht, und auf was dabei zu achten ist.

#### Wann

10.11.2018 - ab 15-17 Uhr

#### Wo

Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Was muss ich mitbringen

* gegebenenfalls einen Laptop, mit VirtualBox
* beliebige Linux Kenntnisse
* Gute Laune

#### Vortragende

yuna

#### Eintritt

Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.

#### Referals

none ATM

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
