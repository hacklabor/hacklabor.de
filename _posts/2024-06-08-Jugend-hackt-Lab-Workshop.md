---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Workshop: NeoPixel LEDs
![Workshop Bunte Lichterwelten](/assets/blog/2024-06-08-Jugend-hackt-Workshop.jpg)

Entdecke die Magie der NeoPixel LEDs und lerne, wie du tolle Lichteffekte selbst programmierst! 🚥

Ganz gleich, ob du schon mal mit Elektronik und Programmierung zu tun hattest oder nicht, wirst du mit einem funktionierendem Ergebnis und deinen eigenen leuchtenden LEDs nach Hause gehen. Also komm vorbei und lass uns gemeinsam die Welt ein bisschen bunter machen! 🌈

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2024-06-15/)

Wir freuen uns auf dich! 🦙

#### Wann?
15.06.2024 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

