---
title: Nacht des Wissens Schwerin
tags: veranstaltung vortrag mitmachen
author: Sebastian
---
# Vorträge und Mitmachen auf der Nacht des Wissens 2023 
## Samstag den 21.10.2023, 17 bis 22 Uhr

Dieses Jahr findet die 8. Nacht des Wissens in Schwerin statt.
Auch das Hacklabor ist wieder mit dabei und  bietet ein spannendes Programm aus Vorträgen und Mitmachaktionen.

Ab 17 Uhr könnt ihr bei unserem spannenden Programm teilnehmen.  
**Der Eintritt ist kostenfrei.**

**UNSER PROGRAMM**

**17 - 22 Uhr**

***Führung durch das Hacklabor***

***Löten für Kinder***   
In diesem Workshop lernst du löten. Deine bunt blinkende 
Kreation kannst du danach mit nach Hause nehmen.

***Lockpicking***  
Du bist neugierig darauf, wie Schlösser funktionieren oder interessierst 
dich vielleicht schon für Lockpicking? Dann bist du hier genau richtig!

***Breakout Spiel***   
Wir haben den Spielklassiker interaktiv gemacht. 
Steuere das Spiel mit deinem Körper und knacke den Rekord.

***3D Drucker in Aktion***   
Schau dir unseren 3D Drucker in Aktion an und lass dir erklären, wie er funktioniert.


**VORTRÄGE**

18:00 Uhr  
**HACKER, HACKERSPACES UND DIE OFFENE WERKSTATT - WAS IST DAS EIGENTLICH?**  
Eine Einführung in die Welt der Hackerspaces, der Hackerkultur und die offene Werkstatt im Hacklabor. 
*Dauer 45 Minuten*

19:00 Uhr  
**WAS KANN K.I.? CHANCEN UND RISIKEN EINER TECHNOLOGIE DER GEGENWART**  
Künstliche Intelligenz boomt wie nie zuvor. Viele Jahre war es relativ still 
um die vermeintlich schlauen Maschinen und das Thema eher Wissenschaftler:innen 
und Computerfreaks vorbehalten. Doch nun sind ChatGPT, Midjourney und Bard 
in aller Munde. Die neuesten Produkte von OpenAI, Google und Co dominieren 
seit Monaten die Nachrichten und sind vollständig im alltäglichen Diskurs 
angekommen. Eine differenzierte Erklärung was diese K.I. eigentlich ist, von 
der alle reden, erfolgt dabei jedoch nur selten. Dieser Vortrag will daher 
nicht nur den Begriff der Künstlichen Intelligenz entmystifizieren, sondern 
ihre Grenzen und tatsächliche Gefahren aufzeigen.  
*Dauer 45 Minuten*

20:00 Uhr  
**OPENBIKESENSOR IN SCHWERIN**  
Das mit dem Deutschen Fahrradpreis 2022 ausgezeichnete OpenSource Projekt 
"OpenBikeSensor" kommt nach Schwerin. Wie funktioniert das eigentlich und 
kann ich da mitmachen? Radentscheid und Hacklabor starten ein gemeinsames 
Projekt!  
*Dauer 45 Minuten*


**INFOS ZUR NACHT DES WISSENS**  
Weitere Infos zur Veranstaltung und den teilnehmenden Einrichtungen findet ihr hier: [https://www.nachtdeswissens-schwerin.de](https://www.nachtdeswissens-schwerin.de/)


**Kommt vorbei, wir freuen uns auf euch!**
