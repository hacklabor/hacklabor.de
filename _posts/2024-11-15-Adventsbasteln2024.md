---
title: Weihnachten im Hacklabor
tags: events, basteln, öffentlich, Advent
author: Gerd
---
# Adventsbasteln 2024
## Samstag den 30.11.2024, Beginn 13.37 Uhr 
![Adventsbasteln2024](/assets/blog/2024-11-15-Adventsbasteln_2024.jpg)

Wie jedes Jahr nähert sich Weihnachten mit großen Schritten. Wieder steht die Frage im Raum: Was soll ich schenken? Nicht zu teuer und am besten mit Liebe selbst gemacht. Deswegen möchten wir euch einladen, um mit uns verschiedene weihnachtliche Lötkits (LED Tannenbaum, Sterne, Herzen und mehr) sowie Weihnachtsdeko aus dem Lasercutter zu basteln.

Für das leibliche Wohl ist mit Plätzchen, selbst gebackenem Kuchen und Kaffee gesorgt.

Am Abend werden wir noch die Feuerschale anmachen (vorbehaltlich Wetter) und den Tag bei Kinderpunsch und Glühwein ausklingen lassen.

Ein weihnachtlicher Nachmittag für die ganze Familie - wir freuen uns auf euren Besuch!

#### Wann?
30.11.2024 - ab 13.37 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

PS: [So kommt ihr zu uns](/standort/)

#### Eintritt
Die Veranstaltung ist wie immer kostenlos. Bringt aber, wenn ihr möchtet, etwas Kleingeld für Getränke und Material mit.









