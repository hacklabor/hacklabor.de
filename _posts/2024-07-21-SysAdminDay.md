---
title: System Administrator Appreciation Day
tags: admin event sysadminday public talk
author: Gerd
---

![SysAdminDay](/assets/blog/2024-07-21-SysAdminDay.png)

Das Netzwerk ist sicher, auf dem PC sind die neuesten Updates installiert und der Drucker läuft ganz ohne Papierstau. Warum ist das so? Richtig, weil es engagierte Systemadmins gibt, die den Laden am Laufen halten.

Zeit, Danke zu sagen und zwar mit dem **System Administrator Appreciation Day, am Freitag den 26.07.2024 ab 19 Uhr im Hacklabor in Schwerin**.

Der SysAdminDay ist ein jährlicher Gedenktag, der von Ted Kekatos erfunden wurde und seit 2000 am letzten Freitag im Juli stattfindet. Kekatos wurde durch eine Anzeige von Hewlett-Packard inspiriert, in der einem Systemadministrator mit Blumen und Obstkörben von Mitarbeitern gedankt wird, weil er neue Drucker installiert hat.

Wir möchten alle Admins und Interessierte an diesem Abend ins Hacklabor im TGZ einladen, um ihre Arbeit zu ehren und sich in gemütlicher Runde auszutauschen. Weiterhin gibt es für alle Gäste ein **Freigetränk, sowie eine kostenlose Bratwurst vom Grill**.

Wir freuen uns auf euren Besuch!

#### Wann?
26.07.2024 - ab 19 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

