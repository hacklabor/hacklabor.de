---
title: Offener Werkstatt Termin im Februar25
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---
So langsam wird es Frühling und die Natur erwacht wieder. Wie wäre es mit einem neuen Zuhause für die geliebten Vögel? Oder vielleicht auch was ganz anderes?

Dann komm doch am 8. Februar 2025 zwischen 12 und 17 Uhr zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stellen dir unser Werkzeug zur Verfügung. Bei Bedarf stehen wir auch mit Tipps und Ratschlägen zur Seite.

#### Wann?
08.02.2025 - von 12 bis 17 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.
