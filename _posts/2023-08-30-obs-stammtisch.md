---
title: OpenBikeSensor Stammtisch
tags: event public openhouse
author: Christoph 'ovnn'
---

Nach dem die ersten mit einem Eigenem OpenBikeSensor in Schwerin unterwegs sind, wird es nun Zeit die ersten Erfahrungsberichte zu teilen. Neben dem Erfahrungsaustauch bietet dieser Event auch die Gelegenheit für neu Interesierte Antworten auf Ihre Fragen zu bekommen. Wir freuen uns auf euch und die Interessanten Gespräche. 

#### Wann?
20.09.2023 - 19 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.

