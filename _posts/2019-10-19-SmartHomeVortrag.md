---
title: Smart HOME Vortrag NDW2019
tags: events
author: ThomasK
---
# Links zum Vortrag Preiswerte (Glüh)Birnen und andere Smart Home-Früchtchen

- [Zigbee (Wikipedia)](https://en.wikipedia.org/wiki/Zigbee)
- [Wireless LAN (Wikipedia)](https://en.wikipedia.org/wiki/Wireless_LAN)
- [Lampe RGB (Amazon)](https://smile.amazon.de/dp/B07RF8BM62/ref=cm_sw_em_r_mt_dp_U_BcgRDb0NV1CQ8)
- [Steckdose (Amazon)](https://smile.amazon.de/dp/B0777BWS1P/ref=cm_sw_em_r_mt_dp_U_1dgRDb1P4JQG4)
- [Sonoff-Tasmota (Github)](https://github.com/arendst/Sonoff-Tasmota)
- [ESPurna Firmware Wiki (Github)](https://github.com/xoseperez/espurna/wiki)
- [tuya.com](https://en.tuya.com/company)
- [Howto: Gosund Steckdose mit Tasmota (Bastelbunker)](https://www.bastelbunker.de/gosund-sp1-mit-tasmota/)
- [TUYA-CONVERT (Github)](https://github.com/ct-Open-Source/tuya-convert)
- [vtrust](https://www.vtrust.de/35c3/) [Video: Smart Home - Smart Hack | 
Wie der Weg ins digitale Zuhause zum Spaziergang wird (media.ccc.de)](https://media.ccc.de/v/35c3-9723-smart_home_-_smart_hack)



Workshop 09.11.2019 ab 14.00 Uhr zum Thema Neue Software aufspielen
