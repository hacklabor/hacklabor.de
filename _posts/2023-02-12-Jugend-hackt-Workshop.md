---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Einführung in die Programmierung mit Python
## Samstag den 18.02.2023, 14 bis 18 Uhr

Du möchtest lernen, wie man programmiert und hast bisher noch keine Erfahrung? Dann bist du in diesem Workshop genau richtig! ⌨️

Python ist eine sehr beliebte, vielfältig einsetzbare und gut zu erlernende Programmiersprache. In diesem Kurs lernst du die Grundlagen zum Programmieren mit Python und wirst am Ende ein kleines einfaches Spiel erstellt haben. 👾

Die ideale Gelegenheit für den Einstieg in die spannende Welt des Coding. 👩‍💻

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2023-02-18/)

🚧 Bitte beachte 🚧: Zur Zeit ist die Zufahrt über die Hagenower Str. gesperrt. Zu Fuß oder mit dem Fahrrad kommst du rechts am Haus 1 vom TGZ vorbei und musst einmal rum gehen. Mit dem Auto erreicht ihr uns über die Mettenheimerstraße.

Der Kurs beginnt um 14 Uhr. Wir freuen uns auf dich! 🦙

#### Wann?
18.02.2023 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

