---
title: Buchvorstellung mit Arne Semsrott
tags: event public talk lesung politik extern
author: Gerd
---

![Arne-Semsrott](/assets/blog/2024-10-09-Arne-Semsrott.png)

### Einladung zur Buchvorstellung: „Machtübernahme – Was passiert, wenn Rechtsextremisten regieren“ mit Arne Semsrott

Es sind beunruhigende Zeiten: Antidemokratische Positionen gewinnen in der Gesellschaft immer mehr an Zustimmung, und die AfD erreicht nicht nur in Wahlumfragen Rekordwerte. Was passiert, wenn Rechtsextremisten tatsächlich an die Macht kommen? Und wie können wir unsere demokratische Gesellschaft davor schützen?

Der renommierte Journalist und Aktivist Arne Semsrott stellt in seinem Buch „Machtübernahme“ diese brennenden Fragen in den Mittelpunkt. Er zeigt eindrücklich, wie unsere Institutionen – Schulen, Behörden, Justiz, öffentlich-rechtlicher Rundfunk – anfällig für rechtsextreme Einflussnahme sind.
Gleichzeitig liefert er konkrete Strategien, wie wir diesen Angriffen begegnen können. Ob durch Generalstreiks, das Leaken von Informationen oder das Remonstrationsrecht für Beamte: Semsrott zeigt Wege auf, wie sich Zivilgesellschaft, Medien, Gewerkschaften und Unternehmen gegen eine rechte Machtübernahme wehren können.

In dieser Lesung und anschließenden Diskussion gibt Arne Semsrott einen tiefen Einblick in die Gefahren, die unsere Demokratie bedrohen, und erläutert, welche Maßnahmen jetzt nötig sind, um sie zu verteidigen.

**Seien Sie dabei und erfahren Sie, welche Wege wir gemeinsam beschreiten können, um unsere Demokratie zu schützen – die Zeit zu handeln ist jetzt!**

Dies ist eine Kooperationsveranstaltung des *Hackspace Schwerin e.V.* und der *Heinrich-Böll-Stiftung MV*.

#### Wann?
08.11.2024 - 19 Uhr

#### Wo?

⚠️ **Achtung:** Diese Veranstaltung findet nicht im Hacklabor sondern im Schweriner Rathaus statt! ⚠️

Demmlersaal \\
Rathaus Schwerin \\
Am Markt 14 \\
19055 Schwerin

#### Eintritt
Die Veranstaltung ist kostenlos. Reservierungen sind über eine kurze, formlose E-Mail an [anmeldung(at)hacklabor.de](mailto:anmeldung@hacklabor.de?subject=Lesung%20Arne%20Semsrott%202024) möglich.

