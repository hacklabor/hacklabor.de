---
title: "Informationsfreiheitsgesetz ftw"
tags: jugendhackt hackathon
author: Micha
image: /assets/blog/ifgftw.png 
---
![Informationsfreiheitsgesetz ftw](/assets/blog/ifgftw.png)

Während die seit Kurzem verbindliche DSGVO die Daten von Privatpersonen schützt, dient das Informationsfreiheitsgesetz dazu, Privatpersonen den Einblick in Dokumente und Akten der öffentlichen Verwaltung zu ermöglichen. Dazu zählen neben Ämtern und Ministerien überraschender Weise unter anderem auch die Sparkassen und Jobcenter. 

Besonders einfach lassen sich entsprechende Anfragen über fragdenstaat.de stellen. Arne Semsrott, Projektleiter dieser mehrfach ausgezeichneten Website, ist am Donnerstag, dem 5. Juli 2018 ab 19 Uhr, zu Gast im Schweriner Hacklabor. Er wird über die aktuell laufenden IFG Meisterschaften, vergeblichen Klagen der Bundesregierung und Highlights aus den über 29.000 Anfragen berichten. Wie eigene Anfragen formuliert werden, erklärt der praktische Teil des Abends.

Das Hacklabor befindet sich im Schweriner Technologiezentrum. Es ist mit dem Bus Nummer 7 bequem zu erreichen und bietet ausreichend Parkplätze für Fahrräder und Autos. Der Eintritt ist frei. Gefördert wird die Veranstaltung durch die Landeszentrale für politische Bildung Mecklenburg-Vorpommern.
