---
title: Radeln für Daten
tags: Fahrrad workshop OpenBikeSensor
author: Christoph 'ovnn'
---

Seit 2020 gibt es einen Mindestabstand beim Überholen von Kfz zu Radfahrern. Innerorts sind es 1,5 Meter, außerorts sogar 2 Meter. Um zu zeigen, wo es in Schwerin besonders kritisch ist, haben das Hacklabor und der Radentscheid im Sommer 2023 gemeinsam ein Projekt gestartet um Daten zu sammeln. 
Hierfür nutzen wir den [OpenBikeSensor](https://www.openbikesensor.org/), er ist ein technisches Gerät am Fahrrad, welches von der OpenBikeSensor Community entwickelt und weiterentwickelt wird.
Er misst den Abstand zum überholenden Kfz und zeichnet die Fahrt auf.
Das Ziel des Projekts ist, Schwachstellen und Verbesserungspotenzial in Schwerin aufzuzeigen und diese in Kooperation mit Stadt- und Verkehrsplanern zu beheben.

![OpenBikeSensor](/assets/blog/obs.jpg)

Über dreißig OpenBikeSesoren wurden bisher in Schwerin gebaut, fast 7.000 Überholvorgänge, 9.000 km Strecke sind dokumentiert und können unter [obs-sn.de](https://obs-sn.de/map) (bitte reinzoomen). öffentlich eingesehen werden. 
Um statistisch verlässliche Daten zu erhalten, ist es wichtig, dass möglichst viele Alltagsradler mit den Sensoren Daten erfassen.

Es gibt regelmäßig Workshop-Termine, in denen das Gerät unter Anleitung selbst zusammengebaut wird (nächster Termin 27.4). Außerdem gibt es einen regelmäßigen Stammtisch zum gemeinsamen Austausch. Dort können offene Fragen geklärt und Erfahrungen ausgetauscht werden. Sowohl neue Interessenten als auch Leute, die den OBS schon nutzen, können dort ins Gespräch kommen. 

Jetzt mithelfen, um Daten zu sammeln und unter [https://forms.gle/2t6cj81KFaAaeuMs5](https://forms.gle/2t6cj81KFaAaeuMs5) Interesse bekunden.
