---
title: Hacklabor im NOWHERE
tags: rc3 project nowhere
author: Sebastian
---

![hacklabor-nowhere](/assets/blog/2021-12-20-hacklabor-nowhere/rc3-2021-header01.jpg)

Auch dieses Jahr kann der Chaos Communication Congress nicht wie gewohnt statt finden.
Dafür gibt es die online stattfindende [rC3](https://events.ccc.de/2021/11/08/rc3-2021-nowhere/).

Wenn auch nur online, bietet die rC3 die Möglichkeit trotzdem etwas Chaos Event Feeling zu sich nach Hause zu holen.
Letztes Jahr hatten wir die Idee ein Spiel für euch auf der rC3 zu bauen.
Daraus wurde “Virus Hunt”. Ihr konntet über das Internet ein physisches Spiel lokal bei uns im Hacklabor spielen und steuern.
Wie immer stellten wir uns die Frage, was machen wir dieses Jahr?
Nochmal das gleiche kommt nicht in Frage, das wäre zu einfach. ;)

Also wurden erstmal Ideen gesammelt und Machbarkeiten ausgelotet.
Unser fleißiges Team arbeitet nun seit einiger Zeit an einem neuen Spiel, bei dem ihr während der rC3 auf Highscore Jagd gehen könnt.

Hier bekommt ihr ein paar Einblicke zum Bau des Projektes:

![rc3_project_01](/assets/blog/2021-12-20-hacklabor-nowhere/IMG_20211121_142930-blog.jpg)

![rc3_project_02](/assets/blog/2021-12-20-hacklabor-nowhere/IMG_20211127_172226-blog.jpg)

![rc3_project_02](/assets/blog/2021-12-20-hacklabor-nowhere/IMG_20211128_090159-blog.jpg)

![rc3_project_02](/assets/blog/2021-12-20-hacklabor-nowhere/IMG_20211128_150229-blog.jpg)
