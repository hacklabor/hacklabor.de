---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Lockpicking
![Workshop Lockpicking](/assets/blog/2023-09-23-Jugend-hackt-Workshop.png)

Du bist neugierig darauf, wie Schlösser funktionieren oder interessierst dich vielleicht schon für Lockpicking? Dann bist du hier genau richtig!

Lass uns gemeinsam die Geheimnisse des Lockpickings erkunden. Wir zeigen dir, wie Schlösser aufgebaut sind und wie du sie mit den richtigen Werkzeugen und Techniken öffnen kannst. Das Verständnis für Schlösser ist nicht nur faszinierend, sondern ermöglicht dir, Sicherheitsmechanismen besser zu verstehen und einzuschätzen.

Egal, ob du Anfänger:in bist oder bereits einige Grundkenntnisse hast, bei uns bist du herzlich willkommen. Unsere Workshops sind interaktiv und machen jede Menge Spaß. Du kannst deine Fähigkeiten ausprobieren und dich mit anderen Gleichgesinnten austauschen.

Bist du bereit, die Herausforderung anzunehmen? Melde dich jetzt schnell und kostenlos für unseren Lockpicking-Workshop an und werde ein:e echte:r Schlüsselmeister:in!

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2023-10-28)

Wir freuen uns auf dich! 🦙

#### Wann?
28.10.2023 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

