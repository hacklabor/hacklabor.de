---
title: "Vortrag: IPv6 - Das unbekannte Wesen"
tags: ipv6 vortrag
author: micha
edit by: claus
---
Am Donnerstag, den 08. März 2018 um 19:00 Uhr, könnt Ihr im Hacklabor mit
Rabbit@Net einen Blick in die IPv6-Welt riskieren. Denn auch wenn es nicht
so aussieht, IPv6 ist schon heute Realität.
Davon merken jedoch die wenigsten etwas; und das ist gut so.

![IPV6 Vortrag im Hacklabor](/assets/blog/ipv6-vortrag-2018.png)

Zunächst wirft der Vortag eine Blick auf den bisherigen Adressierungsstandard 
IPv4 und die Probleme, die es damit gibt. IPv6 verspricht, die Lösung zu sein. 
Nach einen schnellen Blick auf die Neuerungen, widmet sich der Vortrag den 
Adressformat und -typen sowie ihrer Darstellung. Weiter geht es mit einem 
Überblick über die automatische Adresskonfiguration und ihre Ausprägungen.
Es gibt auch praktisch Hinweise zur Einführung von und Umgang mit IPv6 im Heim- 
und Unternehmensnetzwerk. Einige Empfehlungen und Stolpersteine kommen dabei 
zur Sprache. Abschließend macht der Vortrag noch einen kurzen Realitätscheck 
und zeigt, wie weit IPv6 bereits verbreitet ist.

Also kommt zahlreich vorbei oder genießt den Stream. *)

*) nette Gesellschaft, Getränke und Verpflegung nur bei persönlichem Erscheinen. 
Angebot ist begrenzt auf die maximal baupolizeilich zulässige Personenzahl 
in den Vereinsräumlichkeiten.