---
title: Eröffnung des Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Workshop: Regenbogen-Ring am 15.10.2022

Mit diesem Workshop eröffnen wir ganz offiziell das Jugend hackt Lab Schwerin. 🥳

Wir zeigen dir, wie man einen Neopixel LED-Ring zum Strahlen bringt. Ziel ist es, eine kleine Uhr damit selbst zu bauen. Dabei sind deiner Fantasie jedoch keine Grenzen gesetzt und du kannst ebenso eine Temperaturanzeige, ein schöne Zimmerdekoration oder was immer dir einfällt basteln.

![CC BY-NC-SA 4.0, Foto: https://www.instructables.com/member/MrSottong/](/assets/blog/2022-09-13-Eroeffnung-Jugend-hackt-Lab.webp)

Ganz gleich, ob du schon mal mit Elektronik und Programmierung zu tun hattest oder nicht, wirst du mit einem funktionierendem Ergebnis nach Hause gehen. Also komm vorbei und lass uns gemeinsam die Welt ein bisschen bunter machen! 🌈

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2022-10-15/)

Wir freuen uns auf dich! 🦙

#### Wann?
15.10.2022 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

