---
title: Workshop Arduino & Friends - vom Einsteiger bis Experten
tags: arduino stm32 ws2812b microcontroller basteln oled display beginner advanced
author: moe
---

![ArduinoFriends-workshop](/assets/blog/monthly-arduino-workshop.jpeg   )

# Arduino & Friends Workshop

Der Arduino und seine Ableger sind Grundlage vieler Projekte, auch hier im Hacklabor. Unsere Erfahrung möchten wir in einem monatlich stattfindendem Workshop mit euch teilen. Am 07. Oktober um 14:30 Uhr ist der nächste Termin. 

Ihr bringt eure Ideen, Wünsche, Fragen und Hardware mit. Wir haben Arduinos in klein und groß, mit WLAN und ohne. Dazu zahlreiche Sensoren und Aktoren wie Displays, LEDs und Motoren. Laptops zum Programmieren sind ebenfalls vorhanden.

## Anmeldung & Eintritt
Es ist keine Anmeldung erforderlich. Kommt einfach vorbei. Die Veranstaltung ist kostenlos. Bitte bring trotzdem etwas Kleingeld für das eine oder andere mit.

## Wo?
Hacklabor Schwerin, Hagenower Straße 73, 19061 Schwerin

Wir freuen uns über zahlreiches Erscheinen.

PS: [So kommst du zu uns](https://hacklabor.de/standort/) 