---
title: Nacht des Wissens - 14.10.2017
tags: nacht-des-wissens
author: micha
---

![](/assets/blog/nachtdeswissens-eule.png)

Das ist unser Programm zur Nacht des Wissens am 14.10.2017:

### Führung durch das Hacklabor

**17:15 - 17:30 Uhr und 20:00 - 20:15 Uhr**

### Das Darknet - Fluch oder Segen?

Das sogenannte Darknet taucht immer wieder in Medienberichten auf. Meist in einem 
negativen Kontext. In seinem Vortrag geht Michael Milz auf verschiedene Aspekte 
des Darknets am Beispiel vom TOR-Netzwerk ein.

**17:45 Uhr - 18:30 Uhr und 20:30 Uhr - 21:15 Uhr**

### Bitcoin - Das magische Internetgeld

Bitcoin wird hauptsächlich im Darknet verwendet. Als Zahlungsmittel für diverse 
Cyberangriffe in Verruf gekommen, erklärt Matthias Steffen in seinem  Vortrag 
unterschiedliche Facetten dieser Währung.

**19:00 - 19:45 Uhr und 21:15 Uhr - 22:00 Uhr**

Das gesamte Programm gibt es unter [www.nachtdeswissens-schwerin.de](http://www.nachtdeswissens-schwerin.de/).

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
