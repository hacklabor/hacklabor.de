---
title: "Der WLAN Monitor"
tags: wlan wifi monitor security
author: Micha
image: /assets/blog/wlanmonitor.jpg 
---
Was ist das für ein Gefühl, wenn auf einem riesigen Monitor die Namen der WLANs des eigenen Zuhause, der Firma oder des letzten Urlaubes auftauchen und es so aussieht als würde eine Verbindung mit genau diesen Netzwerken hergestellt werden?
![wlanmonitor](/assets/blog/wlanmonitor.jpg)

Im Rahmen der Nacht des Wissens in Schwerin machen wir im Hacklabor genau das. Wie das geht und warum das gefährlich sein kann, erklärt dieser Artikel. 

## Smartphones mit aktivem WLAN

Smartphones sind weit verbreitet und wer eins hat, nutzt es in der Regel in den eigenen vier Wänden auch per WLAN. So spart man Datenvolumen und meist ist die Verbindung auch schneller. Wenn das WLAN auf dem Gerät ersteinmal aktiviert ist, bleibt es das, auch außerhaus.

Damit eine WLAN-Verbindung aufgebaut werden kann, sagt der Access-Point in regelmäßigen Abständen etwas wie: "Ich mache das WLAN mit dem Namen xyz und arbeite mit dieser Verschlüsselung." Diese Daten benötigt das Smartphone um die Verbindung aufzubauen. Damit der Aufbau noch schneller geht, ruft das Smartphone die ganze Zeit: "Ich kenne die WLANs abc, def und xyz." Das hört der Access-Point. Ist ein passendes dabei, versendet er wiederum die notwendigen Infos.

## Der Monitor

Diese Rufe des Smartphones werden von einem [Minicomputer](https://de.wikipedia.org/wiki/Raspberry_Pi) gehört, ausgewertet und auf dem Monitor dargestellt. Natürlich tun wir nur so, als ob wir eine Verbindung zu den WLANs aufbauen.

## Wo ist die Gefahr?

Wenn ich weiß, welches WLAN ein Smartphone kennt, kann sich ein Angreifer als dieses WLAN ausgeben. Wird dann eine Verbindung über das Netzwerk des Angreifers aufgebaut, läuft der gesamte Internetverkehr über die Leitungen des Angreifers. So kann er beispielsweise Zugangsdaten zu E-Mails, Apps und Websites abgreifen oder Daten manipulieren um Viren und Trojaner zu installieren.

## Kann ich mich schützen?

Am einfachsten schützt man sich, wenn man das WLAN nur aktiviert, wenn es wirklich gebraucht wird. Das hat zusätzlich den positiven Effekt, dass der Akku geschont wird.

Zusätzlich sollte man regelmäßig die WLANs ohne Verschlüsselung aus der Liste der bekannten WLANs auf dem Smartphone entfernen.

Ebenso wichtig ist es, darauf zu achten, dass die genutzten [Websites](https://support.mozilla.org/de/kb/wie-kann-ich-feststellen-ob-meine-verbindung-zu-einer-website-verschluesselt-erfolgt) und der [E-Mail-Anbieter](https://www.privacy-handbuch.de/handbuch_31c.htm) Transportverschlüsselung unterstützen und diese auch genutzt wird.

## Disclaimer

Dieser Text richtet sich an die Besucher der Nacht des Wissens und ist bewusst einfach gehalten. Dem Autor ist bewusst, dass es auch Angreiferinnen geben kann. Der WLAN Monitor speichert keine Daten der Besucher. Bei der Vorbereitung wurde darauf geachtet, keine installierten WLAN-Kabel zu beschädigen.  
