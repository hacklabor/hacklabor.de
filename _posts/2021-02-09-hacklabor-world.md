---
title: Digitales Hacklabor
tags: world workadventure public 
author: Gerd
---
# Besucht uns im digitalen Hacklabor

Coronabedingt ist es bedauerlicherweise auch nach wie vor nicht möglich, das Hacklabor mit Leben zu füllen. Nichtsdestotrotz möchten wir weiterhin einen offenen Raum für alle mit Interesse am kreativen Umgang mit Technik bieten. Aus diesem Grund geben wir von nun an die Möglichkeit, das Hacklabor in einer digitalen Welt zu besuchen. 

Unter **[world.hacklabor.de](http://world.hacklabor.de)** steht der Space allen interessierten Wesen rund um die Uhr zur Verfügung. **Jeden Freitag ab 20 Uhr** finden sich hier auch regelmäßig die Mitglieder zu einem öffentlichen Treffen zusammen. Wenn ihr uns kennenlernen oder mit uns in den direkten Austausch gehen möchtet, ist dieser Zeitraum definitiv am besten geeignet.

![preview](/assets/blog/2021-02-09-hacklabor-world/preview.png)

## Anleitung
Nach dem Klick auf [world.hacklabor.de](http://world.hacklabor.de) werdet ihr zunächst darum gebeten, einen Nickname anzugeben. Dies kann euer echter Name oder auch ein Pseudonym sein. Bestätigt eure Eingabe mit Enter.
![name](/assets/blog/2021-02-09-hacklabor-world/name.png)

Als Nächstes wählt ihr einen der verfügbaren Avatare aus oder gestaltet selbst einen. Dieser dient als eure Repräsentation in der digitalen Welt. Erneut bestätigt ihr eure Auswahl mit der Enter-Taste.

![avatar](/assets/blog/2021-02-09-hacklabor-world/avatar.png)

Schließlich werdet ihr aufgefordert, den Zugriff auf euer Mikrofon und eure Kamera zuzulassen. Es ist empfehlenswert, diesen Zugriff zu gewähren und nach Betreten der Onlinewelt Mikrofon und Kamera zunächst zu deaktivieren. Die entsprechenden Schaltflächen befinden sich später am unteren rechten Bildschirmrand: 
![controls](/assets/blog/2021-02-09-hacklabor-world/controls.png)

Auf diese Weise kann euch erstmal niemand sehen oder hören und ihr habt trotzdem noch die Gelegenheit, Mikrofon und Kamera einzuschalten, wenn ihr euch wohl fühlt und mit jemandem reden möchtet.

Fürs Erste jedoch erlaubt die Vorschau in der Mitte nun, eure Einstellungen zu überprüfen. Ein letztes Mal bestätigt ihr mit Enter und schon findet ihr euch in der Onlinewelt vor dem Eingang zum Hacklabor wieder.
![camera](/assets/blog/2021-02-09-hacklabor-world/camera.png)

Euren Avatar steuert ihr mit den Pfeiltasten durch die Welt. Nähert ihr euch einem anderen Teilnehmer wird direkt eine Verbindung aufgebaut und ihr könnt euch unterhalten. Dies wird durch einen weißen Kreis um die Avatare verdeutlicht. Um das Gespräch zu beenden, bewegt ihr euch einfach aus dem weißen Kreis heraus. Auf diese Weise können bis zu vier Gesprächspartner miteinander kommunizieren. Für größere Runden gibt es spezielle Konferenzbereiche, die gesondert markiert sind. Begebt ihr euch in solch ein Gebiet, könnt ihr mit allen Personen sprechen, die sich ebenfalls dort aufhalten. 

![conference](/assets/blog/2021-02-09-hacklabor-world/conference.png)

Zusätzlich sind an besonderen Orten in der Onlinewelt Links hinterlegt, die automatisch eine eingebettete Webseite öffnen, sobald man sie betritt. Auf diese Weise erhaltet ihr weiterführende Informationen zu dem jeweiligen Ort oder Objekt im Hacklabor.

Wie schon Morpheus zu Neo sagte: "Ich kann Dir nur die Tür zeigen. Hindurchgehen musst Du alleine." 

**Kommt vorbei, wir freuen uns auf euch!**
