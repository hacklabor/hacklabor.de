---
title: Rollerball ist online
tags: rc3 project nowhere rollerball
author: Sebastian
---

![hacklabor-nowhere](/assets/blog/2021-12-20-hacklabor-nowhere/rc3-2021-header01.jpg)

Wir haben es geschafft!  
"Rollerball" ist online und ihr könnt es spielen.

Zur [Anleitung](https://rollerball.hacklabor.de/#/instructions)   
oder direkt zum [Spiel](https://rollerball.hacklabor.de/#/)

In der [Galerie](https://rollerball.hacklabor.de/#/gallery) findet ihr ein paar Bilder zum Aufbau.

Also viel Spaß bei der Jagd nach dem Highscore!  
Ach ja, ihr bekommt auch einen rC3 Badge, wenn ihr das Spiel gespielt habt. ;)

Wir sehen uns in der rC3 Welt.

Be excellent to each other.
