---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Workshop: OpenStreetMap
![Workshop OpenStreetMap](/assets/blog/2024-08-14-Jugend-hackt-Workshop.jpg)

Entdecke die Welt von OpenStreetMap und werde ein Teil der OpenData-Bewegung! 🗺️

Egal, ob du schon mal von OpenStreetMap gehört hast oder nicht, in unserem Workshop lernst du, wie du selbst aktiv an einer der größten freien Kartenprojekte der Welt mitwirken kannst. Hier erfährst du, warum OpenData so wichtig ist und wie jede:r Einzelne dazu beitragen kann, die Daten für alle Menschen besser zu machen.

Im Gegensatz zu den Karten großer Firmen wie Google Maps ist OpenStreetMap von der Community für die Community. Das bedeutet, dass jede:r Daten hinzufügen oder korrigieren kann. Und das Beste daran? Es macht riesigen Spaß, dabei mitzumachen! 😃

Ein Highlight unseres Workshops: Wir werden mit der OpenSource-App StreetComplete in der echten Welt unterwegs sein und Datenlücken schließen. Du hast die Möglichkeit, direkt vor Ort Informationen zu sammeln und wirklich zur Verbesserung der Karte beizutragen. Es ist von Vorteil, aber nicht notwendig, ein eigenes Android-Smartphone dabei zu haben.

Also komm vorbei und lass uns gemeinsam die Welt ein Stückchen besser kartieren! 🌍

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2024-08-17/)

Wir freuen uns auf dich! 🦙


#### Wann?
17.08.2024 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

