---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Data Detox
![Workshop Data Detox](/assets/blog/2023-06-01-Jugend-hackt-Workshop.jpg)

In diesem Workshop möchten wir dir helfen, Kontrolle über deine Technik zu erlangen. 💡

Mit einfachen Aufgaben zum Nachdenken und Spielen unterstützen wir dich mit Hilfe eines interaktiven Handbuchs dabei, dir Gedanken über verschiedene Aspekte deines digitalen Lebens zu machen – von deinen Social-Media-Profilen bis hin zu sicheren Passwörtern. 🤔

Wir gehen dabei in vier Abschnitten vor:

– Digitale Privatsphäre: hier geht es um das Reduzieren von Datenspuren und darum, wie Konzerne digitale Profile von uns erstellen 👤

– Digitale Sicherheit: mit Tipps und Tricks zur Erstellung starker und sicherer Passwörter 🔒

– Digitale Zufriedenheit: dieser Abschnitt beschäftigt sich mit dem Suchtpotenzial von Smartphones 📱

– Falschinformationen: ein Leitfaden für den Konsum und das Teilen von Informationen online ℹ️

Falls du ein eigenes Smartphone oder Notebook besitzt, bring es gerne zum Workshop mit. Wenn nicht, ist das aber nicht schlimm und du kannst natürlich trotzdem teilnehmen.

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2023-06-24/)

Der Kurs beginnt um 14 Uhr. Wir freuen uns auf dich! 🦙

#### Wann?
24.06.2023 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

