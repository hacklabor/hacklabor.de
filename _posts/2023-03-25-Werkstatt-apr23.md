---
title: Offener Werkstatt Termin im April
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---

Kein Aprilscherz, der nächste Termin für die Offene Werkstatt ist der 1. April 23 von 12 bis 18 Uhr. 

Du hast eine coole Projektidee, aber dir fehlt die passende Werkstatt oder du hast Lust uns beim Optimieren der Werkstatt zu helfen? Dann komm doch an dem Termin zwischen 12 und 18 Uhr zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stehen bei bedarf mit Tipps und Ratschlägen zur Seite.

Zur Zeit ist die Zufahrt zum TGZ von der Hagenower Straße aus gesperrt. Du erreichst uns aber über die Mettenheimer Straße.
PS: [So kommst du zu uns](/standort/)
