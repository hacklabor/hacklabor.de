---
title: Tag des offenen Hackerspace
tags: event public open hackspace
author: Gerd
---

![open hackspace](/assets/blog/2022-08-23-Tag-des-offenen-Hackerspace.png)

Der Chaos Computer Club (CCC) lädt am 27. August 2022 zum Tag des offenen Hackerspace ein. Wir wollen uns vorstellen, Projekte zeigen, uns für Gäste und alle Interessierten öffnen und vielleicht dabei ein paar Hackermythen zerlegen. Zusammen mit 77 weiteren Hackspaces in sechs Ländern öffnet auch das Hacklabor am Samstag seine Türen.

Hackerspaces sind offene Orte für den kreativen Umgang mit Technik. Dort stehen nicht nur Werkzeuge wie 3D-Drucker und Elektroniklabore bereit, sondern sie bieten auch den Raum, in dem sich Hacker, Maker und Bastler treffen, um sich auszutauschen und gemeinsam an Projekten zu arbeiten.

Wir führen euch an diesem Tag durch das Hacklabor und zeigen euch aktuell laufende Projekte. Ihr könnt unseren Lasercutter und 3D-Drucker in Aktion sehen und bei erfrischenden Getränken gemütlich mit uns zusammen sitzen und uns kennenlernen.

Wir freuen uns auf euren Besuch!

#### Wann?
27.08.2022 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

