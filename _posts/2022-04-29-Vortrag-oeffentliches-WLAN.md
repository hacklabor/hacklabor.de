---
title: Vortrag Mein Weg zum öffentlichen WLAN
tags: wifi talk W-LAN hotspot public
author: Gerd
---

![wifi-talk](/assets/blog/2022-04-29-Vortrag-oeffentliches-WLAN.png)

Volker von Hoeßlin, IT-Systemadministrator und Developer bei den Stadtwerken Schwerin, kommt ins Hacklabor, um mit uns in die technischen Tiefen des öffentlichen W-LANs hinab zu steigen.

Er wird über seinen Werdegang rund um die von ihm entwickelten WLAN-Hotspot Systeme berichten, um diese anschließend zu evaluieren und einen Blick in die Zukunft zu werfen.

Anschließend ist selbstverständlich Raum für Fragen und eine Diskussion zum Thema.

#### Wann?
06.05.2022 - ab 20 Uhr /
Dauer ca. 1 Stunde

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

**UND**

Online im Stream auf [twitch.tv/hacklabor](https://www.twitch.tv/hacklabor)

#### Eintritt

Die Veranstaltung ist wie immer kostenlos.

Wir freuen uns auf Dich!

