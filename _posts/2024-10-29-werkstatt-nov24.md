---
title: Offener Werkstatt Termin im November
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---
Die Weihnachtszeit steht vor der Tür und du hast eine coole Bastelidee, die du umsetzen willst? Oder wie cool wäre es, einen selbst designten Schwibbogen zu haben?

Dann komm doch am 9. November 2024 zwischen 12 und 17 Uhr zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stellen dir unser Werkzeug zur Verfügung. Bei Bedarf stehen wir auch mit Tipps und Ratschlägen zur Seite.


#### Wann?
09.11.2024 - von 12 bis 17 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist natürlich wie immer kostenlos.
