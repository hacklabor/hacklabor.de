---
title: Offener Werkstatt Termin im Februar
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---

Hallo, du hast eine coole Projektidee, aber dir fehlt die passende Werkstatt? Dann komm doch am 11.2.23 zwischen 12 und 18 Uhr zusammen mit deinem Projekt zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stehen bei bedarf mit Tipps und Ratschlägen zur Seite.

Zur Zeit ist die Zufahrt zum TGZ von der Hagenower Straße aus gesperrt. Du erreichst uns aber über die Mettenheimer Straße.        
PS: [So kommst du zu uns](/standort/)
