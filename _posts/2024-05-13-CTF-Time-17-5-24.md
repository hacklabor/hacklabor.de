---
title: CTF Time
tags: event
author: Thomas
---
# Capture The Flag Time im Hacklabor
![](/assets/blog/JungeMitFahne.jpeg)


Liebe Hacker-Community,

Wir laden euch herzlich zum [CTFTime.org Event](https://ctftime.org/event/2252) im Hacklabor ein! 
Capture the Flag (CTF) ist ein spannendes Konzept aus der Welt der IT-Sicherheit, bei dem ihr eure Hacking-Fähigkeiten auf die Probe stellen könnt.
Ob ihr bereits Wissen in diesem Bereich habt spielt keine Rolle.

Was ist ein CTF?

Ein CTF ist ein Wettbewerb, bei dem die Teilnehmer verschiedene Herausforderungen lösen müssen, um eine versteckte "Flagge" (eine Zeichenkette) zu finden.
Das Ziel ist es, durch kreatives Denken, Problemlösungskompetenz und technisches Wissen die "Flaggen" zu erobern.

Dieses Wochenende beim [BYUCTF 2024](https://ctfd.cyberjousting.com/) werden folgende Herausforderungen zu meistern sein.

- Kryptographie
- Webanwendungen
- Reverse Engineering
- Forensic
- OpenSourceIntelligence (OSINT)
- IOT 

Das bietet eine großartige Möglichkeit, praktische Erfahrungen im Bereich der Cybersicherheit zu sammeln und eure Fähigkeiten auf spielerische Art zu verbessern.

Egal ob Neuling, Anfänger oder Fortgeschrittener, bei CTF-Events ist für jeden etwas dabei. 

Wir freuen uns auf eure Teilnahme und einen spannenden Abend im Hacklabor! Lasst uns gemeinsam die Grenzen des Hackings ausloten und neue Fähigkeiten erlernen. 

Bis bald beim CTF Time Event!
Euer Hacklabor-Team

#### Wann?
17.05.2024 - ab 16 Uhr und solange wir gemeinsam Spaß haben

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung wie immer kostenlos. Spenden für den Verein sind erwünscht.


