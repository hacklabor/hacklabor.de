---
title: 6. Nacht des Wissens
tags: veranstaltung vortrag mitmachen 
author: Sebastian
---
# Vorträge und Mitmachen auf der 6. Nacht des Wissens
## Samstag den 16.10.2021, 17 bis 22 Uhr

Dieses Jahr findet die 6. Nacht des Wissens in Schwerin statt.
Das Hacklabor ist eine von 18 teilnehmenden Einrichtungen.

Ab 17 Uhr könnt ihr bei Vorträgen und verschiedenen Mitmachaktionen teilnehmen.  
Der Eintritt ist kostenfrei.

**UNSER PROGRAMM**

**17 - 22 Uhr**

***Führung durch das Hacklabor***

***Licht***   
Der Bastelworkshop lässt Dir ein Licht aufgehen. Löten, Bauen und Mitnehmen.

***Open Bike Sensor***   
Es wird der Open Bike Sensor vorgestellt.


**VORTRÄGE**

18:00 Uhr  
**Hacker und Hackerspaces – Was ist das eigentlich?**  
Eine Einführung in die Welt der Hackerspaces und der Hackerkultur.  
*Dauer 45 Minuten*

19:00 Uhr  
**Smart City – Eine Einführung.**  
Smart City ist nach Digitalisierung einer dieser Begriffe, die in vieler Munde sind, aber unter denen sich keiner so recht etwas vorstellen kann und es viele unterschiedliche Sichtweisen gibt. Dies wird hier versucht zu entwirren und zu klären, was das mit Open Data zu tun hat.  
*Dauer 45 Minuten*

20:00 Uhr  
**Cyber**  
Ein Tauchgang in den Cyberspace.  
*Dauer 45 Minuten*


**INFOS ZUR NACHT DES WISSENS**  
Weitere Infos zur Veranstaltung und den teilnehmenden Einrichtungen findet ihr hier: [https://www.nachtdeswissens-schwerin.de](https://www.nachtdeswissens-schwerin.de/)

*Bitte informiert Euch vor dem Start über die
aktuell geltenden Corona-Regeln in Schwerin.*  
***INFO: Maskenpflicht und 3G-Regel an den 18 Standorten***

**Kommt vorbei, wir freuen uns auf euch!**
