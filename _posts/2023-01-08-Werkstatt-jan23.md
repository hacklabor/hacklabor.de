---
title: Offener Werkstatt Termin im Januar
tags: event public openhouse basteln Werkstatt
author: Christoph 'ovnn'
---
# Offener Werkstatt Termin im Januar 23

Hallo, du hast eine coole Projektidee, aber dir fehlt die passende Werkstatt? Dann komm doch am Samstag den **14.1.23 zwischen 12 und 18 Uhr** zusammen mit deinem Projekt zu uns in die Werkstatt. Du kannst dann bei uns an Deinem Projekt arbeiten und wir stehen bei bedarf mit Tipps und Ratschlägen zur Seite.
        
PS: [So kommst du zu uns](/standort/)
