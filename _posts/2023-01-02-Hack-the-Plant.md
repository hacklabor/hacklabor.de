---
title: Hack the Plant
tags: pflanzen hydroponisch hacklaborprojekt
author: Th0m4sK
---
# Testaufbau für hydroponischen Pflanzenanbau
![](/assets/blog/2022-12-20-Hack-the-Plant/anzucht.png)

## Letztes LIVE Bild
![](/htp.png)


## was bisher geschah

### 02.01.2023

- [![Die letzten 7 Tage im Zeitraffer](/assets/blog/2022-12-20-Hack-the-Plant/timlapseKW1.png)](https://youtu.be/yt3YypztZ1Y)

### 27.12.2022

- Bilder sind immer noch schwarz. Ab ca 2.00Uhr wird nachdem die weiße LED ausgegangen ist für 1s die Planzbeleuchtung eingeschaltet. 
- sozusagen ein Postlight (fragt mich nicht) 
- Seitdem sind die Bilder auch in der Nacht i.O.
- [![Upload erstes Zeitraffer Video auf Youtube](/assets/blog/2022-12-20-Hack-the-Plant/firstTimelapse.png)](https://www.youtube.com/watch?v=2zU_23YMOF0)

### 26.12.2022

- Bilder in Nacht teilweise schwarz, sobald das Planzlicht nicht mehr an ist aber die weiße LED auf dem CAM funktioniert
- Pre Ligth der weissen LED von 0.8s auf 5.0s erhöht

### 24.12.2022

- neue Trägerplatte für Mini Gewächshaus
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/cad_Anzuchtplatte.png" alt="Anzuchtplatte CAD" width="600"/> 
- Wasser aufgefüllt und aufbereitet
- PH von 8 auf 6 PH mit [BioBizz Down](https://www.biobizz.com/producto/biodown/)
- EC von 0,3 auf 0,7 erhöht (Flüssigdünger)

- <img src="/assets/blog/2022-12-20-Hack-the-Plant/bio_down.png" alt="Bio Biss Down" width="300"/> 
<img src="/assets/blog/2022-12-20-Hack-the-Plant/duenger0.jpg" alt="Green Hydro-Erntereif Nährlösung" width="300"/>
<img src="/assets/blog/2022-12-20-Hack-the-Plant/duenger.jpg" alt="Inhaltsstoffe" width="300"/>


### 23.12.2022

- einrichten des Python Scriptes für die Timplapse Aufnahmen auf dem Hacklabor Hautomation Server
- kopieren des aktuellen Bildes auf die Hacklabor Webseite
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/server.png" alt="Server" width="300"/>

### 22.12.2022
- Elektronik Prototyp zur Steuerung von Licht und Webcam
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/elektronik1.jpg" alt="elektronik1" width="300"/>
 <img src="/assets/blog/2022-12-20-Hack-the-Plant/elektronik2.jpg" alt="elektronik2" width="300"/>

### 21.12.2022
- Blätter sind zu sehen
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/221221-Keimling.jpg" alt="keimlinge" width="300"/>

### 20.12.2022
- erste kleine Pflanzen
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/221220-Keimling.jpg" alt="keimlinge" width="300"/>


### 18.12.2022

- kein Mangel an high Tech Schaltzeituhren im Hacklabor, dadurch erstmal die Programmierung gespart.
- Prototyp der Lampe ist fertig
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/lampe1.jpg" alt="Lampe" width="300"/>
<img src="/assets/blog/2022-12-20-Hack-the-Plant/lampe2.jpg" alt="Lampe1" width="300"/>
<img src="/assets/blog/2022-12-20-Hack-the-Plant/lampe3.jpg" alt="Lampe1" width="300"/>
<img src="/assets/blog/2022-12-20-Hack-the-Plant/lampe4.jpg" alt="Lampe1" width="300"/>

### 17.12.2022
- die Samen keimen
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/keimlinge.jpg" alt="keimlinge" width="300"/>

### 16.12.2022
- Dünger & Messgeräte für PH EC TDS sind gekomen
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/zeug.jpg" alt="zeug" width="300"/>

### 15.12.2022
- Aussaat von Salat
- <img src="/assets/blog/2022-12-20-Hack-the-Plant/aussaat.jpg" alt="keimlinge" width="300"/>







[def]: https://youtu.be/2zU_23YMOF0