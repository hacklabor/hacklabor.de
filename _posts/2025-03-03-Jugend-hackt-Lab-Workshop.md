---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Anma & Gerd
---

# Workshop: Nestflix
![Workshop Nestflix](/assets/blog/2025-03-03-Jugend-hackt-Workshop.jpg)

## Nestflix – Der smarte Nistkasten für Vögel

Bist du begeistert von Technik und Natur? Hast du Lust, die Welt ein bisschen besser zu machen und Jungvögeln ein sicheres Zuhause zu geben? Dann bist du hier genau richtig! 🛠️🌿

In diesem Workshop bauen wir gemeinsam einen Nistkasten für Vögel, der technisch so ausgestattet ist, dass du einen Blick ins Nest werfen kannst, ohne die Vögel oder ihre Jungen zu stören. 📷🐦

Mit einem Nistkasten aus Holz bietest du den Vögeln einen sicheren Raum, um ihre Eier zu legen und ihre Jungen aufzuziehen. Gerade nach dem Winter suchen viele Singvögel nach einem geeigneten Ort für ihr Nest. Dein Nistkasten kann ein perfektes Zuhause für sie bieten. 🪺🐣

Im ersten Workshop-Teil bauen wir den Nistkasten zusammen und besprechen, wie und wo du ihn am besten anbringen kannst. Während des Nestbaus und der Brutzeit sollten die Vögel nicht gestört werden. Doch wie kannst du trotzdem beobachten, was im Inneren passiert? Dafür installieren wir im zweiten Workshop-Teil eine WLAN-fähige Kamera, mit der du die Vögel live beobachten kannst – ganz bequem von deinem Smartphone oder Computer aus! 🎥🛜

Egal, ob du bereits Erfahrung hast oder dich ganz neu mit den Themen beschäftigen möchtest – bei uns bist du herzlich willkommen. Hier kannst du Neues lernen, deine Fähigkeiten ausprobieren und dich mit Gleichgesinnten austauschen.

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2025-03-15/)

Wir freuen uns auf dich! 🦙😊

#### Wann?
15.03.2025 - 14 bis 18 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

