---
title: Vortrag Echtzeitbildverarbeitung mit JS
tags: web js php
author: moe
---

[![Schrittmuster-Vortrag-Paul](/assets/blog/Schnittmuster.png)](https://www.facebook.com/events/2190464864520790/)


Bildverarbeitung in JavaScript - Im Rahmen des Projektes "Schnittmuster" wurde eine echtzeitfähige Bildverarbeitungsanwendung geschrieben, die es ermöglicht die Bewegung mehrerer Personen simultan nachzuzeichnen.


Dies erklärt euch Paul

#### Wann?
04.08.2018 - ab 14-16 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Was muss ich mitbringen?
* keine oder beliebige Programmierkenntnisse
* jemand mit einem hellen Shirt
* jemand mit einem dunklen Shit

* Gute Laune


#### Vortragender 

Paul 

#### Eintritt

Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.


#### Referals

Vortrag von HS Wismar 


Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)

