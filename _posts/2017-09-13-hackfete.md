---
title: Hackfete - 1 Jahr Hacklabor
tags: eroeffnung
author: micha
---

Ein Jahr gibt es das Hacklabor jetzt. Das möchten wir zusammen mit euch feiern. 
Am Samstag, dem 23. September ab 13:37 Uhr warten Spannung, Spiel, Spass und 
Unterhaltung auf dich.

![Eröffung](/assets/blog/hackfete/Hackfete2017_2-03.png)

Gerne stehen dir unsere Mitglieder Rede und Antwort bei Fragen zum Hacklabor und
unseren Lieblingsthemen.

Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.

PS: [So kommst du zu uns](/standort/)
