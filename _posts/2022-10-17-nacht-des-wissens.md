---
title: 7. Nacht des Wissens
tags: veranstaltung vortrag mitmachen
author: Sebastian
---
# Vorträge und Mitmachen auf der 7. Nacht des Wissens 2022
## Samstag den 22.10.2021, 17 bis 22 Uhr

Dieses Jahr findet die Nacht des Wissens in der 7. Auflage in Schwerin statt.
Auch das Hacklabor ist wieder eine von 26 teilnehmenden Einrichtungen.

Ab 17 Uhr könnt ihr bei Vorträgen und verschiedenen Mitmachaktionen teilnehmen.  
**Der Eintritt ist kostenfrei.**

**UNSER PROGRAMM**

**17 - 22 Uhr**

***Führung durch das Hacklabor***

***Ein leuchtendes Alpaka***   
In diesem Workshop bastelst du das bunt blinkende Maskottchen von
Jugend hackt. Löten, Bauen und Mitnehmen.


**VORTRÄGE**

18:00 Uhr  
**Hacker, Hackerspaces und Jugend hackt - Was ist das eigentlich?**  
Eine Einführung in die Welt der Hackerspaces, der Hackerkultur und in die
Jugendförderung.  
*Dauer 45 Minuten*

19:00 Uhr  
**Sicherer unterwegs im Internet - 5 einfache Maßnahmen zum Schutz
vor Cyberkriminellen**  
Ob Phishing, Trojaner oder Ransomware – die Kriminalität im Internet
boomt seit Jahren. Oftmals verlieren die Betroffenen solcher Angriffe dabei
nicht nur Geld sondern - schlimmer noch - ihre persönlichen Daten. Doch
wie kann man sich davor schützen? Dieser Vortrag stellt die wichtigsten
Gefahren vor und gibt Tipps für wirksame Gegenmaßnahmen.  
*Dauer 45 Minuten*

20:00 Uhr  
**WLAN Positionstracking - der Spion in deiner Hosentasche**  
Smartphones verraten ihrer Umgebung weitaus mehr über ihre Besitzer
als diesen lieb ist. Wie genau das funktioniert und was man dagegen tun
kann, wird in diesem Vortrag erläutert.  
*Dauer 45 Minuten*


**INFOS ZUR NACHT DES WISSENS**  
Weitere Infos zur Veranstaltung und den teilnehmenden Einrichtungen findet ihr hier: [https://www.nachtdeswissens-schwerin.de](https://www.nachtdeswissens-schwerin.de/)


**Kommt vorbei, wir freuen uns auf euch!**
