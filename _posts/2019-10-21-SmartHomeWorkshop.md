---
title: Workshop
tags: events workshop smart home nocloud
author: geekawhyte
---
# Smart Home Geräte raus aus der Cloud

Ihr habt vielleicht unseren Vortrag zu Smart Home in der Nacht des Wissens 2019 gesehen und / oder wolltet schon immer eure Steckdosen oder Glühlampen vom Cloudzwang befreien, wisst aber nicht wie? Dann besucht uns mit euren Geräten am 9.11.2019 von 14 bis 17 Uhr im Hacklabor. Wir zeigen euch, wie ihr eine alternative Firmware aufspielen, konfigurieren und nutzen könnt.

Da der Erfolg von Chipsatz und Originalfirmware der Geräte abhängig ist, können wir euch nichts versprechen, aber wir werden mit euch alles uns mögliche ausprobieren.

Disclaimer: Ihr solltet euch des Risikos bewusst sein, dass auch bei größter Vorsicht mal etwas kaputt gehen kann. Daher können wir für eventuell eintretende Beschädigungen keine Haftung übernehmen.

Die Veranstaltung ist kostenfrei. Bringt aber ggf. etwas Kleingeld für Getränke mit.

PS: [So kommt ihr zu uns](/standort/)
