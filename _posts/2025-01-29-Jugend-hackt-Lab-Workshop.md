---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Anma & Gerd
---

# Winterferien-Workshop
![Workshop mBot2](/assets/blog/2025-01-29-Jugend-hackt-Workshop.jpg)

## Roboter-Action mit dem mBot2 – Bauen, Programmieren, Teamwork!
Hast du Lust, in den **Winterferien** in die Welt der Roboter-Programmierung einzutauchen und gemeinsam mit anderen coole Technik auszuprobieren? 🚀 Dann bist du in unserem **zweitägigen Ferien-Workshop** genau richtig!

🤖 In diesen beiden Workshop-Terminen wirst du in deinem Team einen eigenen **mBot2-Roboter** zusammenbauen und programmieren. Dabei lernst du, wie Roboter sich in ihrer Umgebung orientieren, Hindernisse erkennen oder selbstständig einer Linie folgen können.

🌈 Lass den mBot2 auf einer vordefinierten Bahn fahren, in Regenbogenfarben blinken, coole Sounds abspielen oder auf dein Kommando stoppen – und was passiert eigentlich, wenn sich zwei Roboter unerwartet begegnen? Lass es uns gemeinsam herausfinden.

### Tag 1 (Montag): Zusammenbauen & Grundlagen der Programmierung

🛠️ Du startest damit, euren mBot2 als Team zusammenzusetzen und seine Sensoren, Motoren und LEDs kennenzulernen. Anschließend programmiert ihr erste Bewegungsabläufe, bringt ihn zum Blinken oder lasst ihn coole Sounds abspielen.

### Tag 2 (Dienstag): Herausforderungen & Teamwork

👩‍💻 Jetzt wird es richtig spannend! Du gibst deinem klein mBot2 immer anspruchsvollere Aufgaben und testest, wie eure Roboter miteinander interagieren können. Findet heraus, wie ihr sie so programmiert, dass sie Hindernissen ausweichen, Signale austauschen oder gemeinsam ein Ziel erreichen. Arbeitet als Team zusammen, um die kniffligen Herausforderungen zu lösen!

Egal, ob du schon Erfahrung mit Programmierung hast oder dich zum ersten Mal damit beschäftigst – hier kannst du Neues lernen, kreativ werden und mit anderen Technikbegeisterten Spaß haben.

🦙 Wir freuen uns darauf, mit dir zu bauen, zu programmieren und gemeinsam spannende Roboter-Abenteuer zu erleben!

Da wir nur eine begrenzte Anzahl von Plätzen haben, bitten wir dich um eine kurze und kostenlose Anmeldung ganz einfach unter diesem Link:

[Hier geht’s zur Anmeldung](https://anmeldung.jugendhackt.org/schwerin/2025-02-03/)


#### Wann?
03.02.2025 - 16 bis 20 Uhr

und

04.02.2025 - 16 bis 20 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

