---
title: Mit Ribs & Wings die Big Brother Awards geniessen
tags: bigbrotheraward ribs&wings hacklaborevent
author: Thomas
---
![](https://digitalcourage.de/sites/default/files/styles/slideshow/public/bba_2016_-_bild_krake_bunt_-_heiko_sakurai_mit_bba-logo.jpg?itok=lBXtsYAG)


[https://digitalcourage.de/blog/2017/bigbrotherawards-am-5-mai-2017-seien-sie-dabei](https://digitalcourage.de/blog/2017/bigbrotherawards-am-5-mai-2017-seien-sie-dabei)


__Deshalb laden wir dich am Freitag, den 5 Mai ab 17:00 Uhr zum Ribs & Wings Grillen und gemeinsamen Big Brother Awards schauen und wer dann noch kann, bekommt zum Nachschlag noch eine kleine technische Demonstration!__

Wir freuen uns auf Dich!

![](/assets/blog/2017-05-05-RW-BB/rips.png)
![](/assets/blog/2017-05-05-RW-BB/demonst.png)

PS: [So kommst du zu uns](/standort/)

