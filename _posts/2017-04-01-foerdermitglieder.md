---
title: Hacker hacken Hacklabor
tags: verein
author: Vorstand
---

Zuletzt hatten Mitglieder des Vereins noch für mehr Cybersicherheit gesorgt, in dem sie bei der Beseitigung von Sicherheitslücken in Industrieanlagen und bei Finanzdienstleistern halfen. Nun ist das Hacklabor selbst Opfer eines Hackerangriffes geworden.

Nach bisherigen Untersuchungen gehen die Cyberwehr-Experten davon aus, dass der oder die Angreifer sich ein umfassendes Bild vom Verein gemacht haben. Dazu geeignet sind die regelmäßigen öffentlichen Treffen und die zahlreichen Workshops im Hacklabor. Da die Mitglieder gerne Rede und Antwort stehen, ist es auch für Gäste leicht, wertvolle Informationen zu erhalten.

Offenbar wurde von der Online-Internet-Webseite die Big-Data-Datenbanken entwendet, in der Angaben zu Fördermitgliedern des Vereins gespeichert sind. Erst nach einer aufwendigen Entschlüsselung der entwendeten Daten dürfte den Angreifern klar werden, dass sie eine leere Datenbank entwendet haben. Der noch junge Verein hat bisher noch keine Fördermitglieder gewonnen.

F. Nord, Mitglied des Vereins, zu dem Vorfall: "Offenbar haben es die Cyberkriminellen auf die Fördermitgliedern des Vereins abgesehen. Das können Einzelpersonen aber auch Firmen sein, die mit ihrem Beitrag die Vereinsarbeit unterstützen."

Die Spur der Hacker konnte mit Hilfe der Weltraumtheorie durch die Cloud und das Darknet bis in den Cyberraum im Neuland zurückverfolgt werden. Dort wurde eine präparierte Website vorgefunden. Diese enthielt die blinkende Nachricht "Support your local Hackspace! Become 1 Fördermitglied!!!!111elf¡" - zusätzlich wurde ein 7-Sekunden-Countdown eingeblendet. Nach Ablauf des Countdowns zerstörte sich die Website selbst und leitete auf folgende URL weiter: [https://hacklabor.de/mitgliedsantrag.pdf](https://hacklabor.de/mitgliedsantrag.pdf)

![Support your local Hackspace! Become 1 Fördermitglied!!!!111elf¡](/assets/blog/hacklabor1april.png)

H. Acker, ebenfalls Vereinsmitglied, freut sich über die moralische Unterstützung: "Aktuell decken die Mitgliedsbeiträge die laufenden Kosten. Durch stetige Beiträge von Fördermitgliedern könnten weitere Geräte oder Arduino Kits für Workshops angeschafft werden."

Der Verein hat noch nicht entschieden, ob der Vorfall bei der Cyberpolizei zur Anzeige gebracht wird. Fragen zum Verein und einer Fördermitgliedschaft beantwortet der Vereinsvorstand unter inbox@hacklabor.de.