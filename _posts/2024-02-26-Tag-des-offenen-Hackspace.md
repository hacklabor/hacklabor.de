---
title: Tag des offenen Hackspace
tags: event public open hackspace
author: Gerd
---

![open hackspace](/assets/blog/2024-02-26-Tag-des-offenen-Hackspace.png)

Der Chaos Computer Club (CCC) lädt am 02. März 2024 zum [Tag des offenen Hackspace](https://events.ccc.de/2024/02/23/tag-des-offenen-hackspace-einladung/) ein. Wir wollen uns vorstellen, Projekte zeigen, uns für Gäste und alle Interessierten öffnen und vielleicht dabei ein paar Hackermythen zerlegen. Zusammen mit [über 30 weiteren Hackspaces](https://md.ccc-mannheim.de/s/tdoh-spaces#) öffnet auch das Hacklabor am Samstag seine Türen.

Hackerspaces sind offene Orte für den kreativen Umgang mit Technik. Dort stehen nicht nur Werkzeuge wie 3D-Drucker und Elektroniklabore bereit, sondern sie bieten auch den Raum, in dem sich Hacker, Maker und Bastler treffen, um sich auszutauschen und gemeinsam an Projekten zu arbeiten.

Wir führen euch an diesem Tag durch das Hacklabor und zeigen euch aktuell laufende Projekte. Ihr könnt unseren Lasercutter und 3D-Drucker in Aktion sehen und bei erfrischenden Getränken gemütlich mit uns zusammen sitzen und uns kennenlernen.

Zeitgleich möchten wir zusammen mit euch unseren 8. Geburtstag feiern. Ein Grund mehr, bei uns vorbei zu kommen!

Wir freuen uns auf euren Besuch!

#### Wann?
02.03.2024 - 13:37 bis 23:42 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

