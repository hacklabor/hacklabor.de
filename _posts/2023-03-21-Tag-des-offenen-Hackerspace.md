---
title: Tag des offenen Hackerspace
tags: event public open hackspace
author: Gerd
---

![open hackspace](/assets/blog/2023-03-21-Tag-des-offenen-Hackerspace.png)

Der Chaos Computer Club (CCC) lädt am 25. März 2023 zum [Tag des offenen Hackerspace](https://www.ccc.de/de/updates/2023/intopenhackerspaces) ein. Wir wollen uns vorstellen, Projekte zeigen, uns für Gäste und alle Interessierten öffnen und vielleicht dabei ein paar Hackermythen zerlegen. Zusammen mit [über 60 weiteren Hackspaces](https://umap.openstreetmap.fr/de/map/tag-der-offenen-hackspaces_786704#8/53.992/11.602) öffnet auch das Hacklabor am Samstag seine Türen.

Hackerspaces sind offene Orte für den kreativen Umgang mit Technik. Dort stehen nicht nur Werkzeuge wie 3D-Drucker und Elektroniklabore bereit, sondern sie bieten auch den Raum, in dem sich Hacker, Maker und Bastler treffen, um sich auszutauschen und gemeinsam an Projekten zu arbeiten.

Wir führen euch an diesem Tag durch das Hacklabor und zeigen euch aktuell laufende Projekte. Ihr könnt unseren Lasercutter und 3D-Drucker in Aktion sehen und bei erfrischenden Getränken gemütlich mit uns zusammen sitzen und uns kennenlernen.

🚧 Bitte beachte 🚧: Zur Zeit ist die Zufahrt über die Hagenower Str. gesperrt. Zu Fuß oder mit dem Fahrrad kommst du rechts am Haus 1 vom TGZ vorbei und musst einmal rum gehen. Mit dem Auto erreicht ihr uns über die Mettenheimerstraße.

Wir freuen uns auf euren Besuch!

#### Wann?
25.03.2023 - 13:37 bis 23:42 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

