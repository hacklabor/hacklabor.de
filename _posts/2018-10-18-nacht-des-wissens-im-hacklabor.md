---
title: "Nacht des Wissens im Hacklabor"
tags: nacht-des-wissens hacklaborevent
author: Micha
image: /assets/blog/nachtdeswissens2018.png 
---
![Informationsfreiheitsgesetz ftw](/assets/blog/nachtdeswissens2018.png)

Auch das Hacklabor ist am kommenden Samstag als Standort an der [Nacht des Wissens](http://www.nachtdeswissens-schwerin.de/) beteiligt. Bei uns erwartet Dich Folgendes:

Bau dir doch eine schöne blinkende Klammer bei uns im Hacklabor und trage das bunte Licht in die Nacht des Wissens. Dieser Workshop ist garantiert einsteigerfreundlich und du kannst jederzeit teilnehmen.

Mit der Ausstellung Transition schauen wir gleichzeitig nach vorne und zurück.

## Vorträge

### 18 Uhr - Die anderen Suchmaschinen.

Wie alles, was mit dem Internet verbunden ist, auch gefunden wird. Welche Folgen kann das haben und wie schützt man sich?

### 19 Uhr - Passwortsicherheit

Der richtige Umgang mit Passwörtern
Überall benötigt man heute Passwörter. Wie soll ich mir all die Kennwörter merken? Wie gehe ich am besten mit Kennwörtern um?

### 20 Uhr - Politik & Transparenz

Reichen schön geschriebene Pressemitteilungen aus, um Wählern den notwendigen Einblick in politische Prozesse und Entscheidungsgrundlagen zu geben? Der Vortrag zeigt am Beispiel von Schwerin und MV Möglichkeiten auf, sich im Cyberraum umfassender zu informieren

Die Veranstaltung ist kostenlos.
