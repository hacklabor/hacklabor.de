---
title: Absage Gesellschaftsspiele Nachmittag im Hacklabor
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Absage: Gesellschaftsspiele Nachmittag im Hacklabor

Leider müssen wir den für nächsten Samstag  (6. August 2022) geplanten Gesellschaftsspiele Nachmittag im Hacklabor absagen.
