---
title: Jugend hackt Lab Schwerin
tags: event jugendhackt jugendhacktlab workshop
author: Gerd
---

# Workshop: Textadventure
![Workshop Textadventure](/assets/blog/2024-02-16-Jugend-hackt-Workshop.png)

Du möchtest gerne selbst Geschichten erzählen und gleichzeitig die Grundlagen des Programmierens erlernen? Das ist genau das, was Textadventures ermöglichen!

Diese rein textbasierten Spiele bieten eine einzigartige Erfahrung, bei der du Entscheidungen triffst, Rätsel löst und die Handlung vorantreibst – alles durch die Kraft deiner Vorstellungskraft und deiner Worte. Wie die ersten Computerspiele ohne Grafik erfordern Textadventures nur deine Fantasie und dein Geschick im Schreiben, um eine immersive und packende Erfahrung zu erschaffen.

Egal, ob du Anfänger:in bist oder bereits einige Grundkenntnisse hast, bei uns bist du herzlich willkommen. Unsere Workshops sind interaktiv und machen jede Menge Spaß. Du kannst deine Fähigkeiten ausprobieren und dich mit anderen Gleichgesinnten austauschen.

Eine Anmeldung ist nicht erforderlich. Komm einfach um 14 Uhr vorbei und lass uns gemeinsam kreative Geschichten erleben und neue Welten erkunden!

Wir freuen uns auf dich! 🦙

#### Wann?
17.02.2024 - ab 14 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist für Jugendliche zwischen 12 und 18 Jahren und natürlich wie immer kostenlos.

#### Webseite
[Jugend hackt Lab Schwerin](https://jugendhackt.org/lab/schwerin/)

