---
title: "Kinoabend \"All Creatures Welcome\" 🎦🎥🎞📽"
tags: hacklaborevent kinoabend congress
author: Murdoc
---
Wir zeigen die Dokumentation All Creatures Welcome von Sandra Trostel. Diese handelt über die Nerdkultur im Zusammenhang mit dem Chaos Communication Camp 2015 und den 32. Chaos Communication Camp (#32C3).
![ACW_Poster](/assets/blog/ACW_Poster.jpg)

Wir kommen gerade frisch vom #35C3 und am Ende des Filmes würden wir auch noch von unseren Erlebnissen des Chaos Communication Congress bereichten.

# Wann

2019-01-12 - ab 20:15 Uhr

# Wo

Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

# Eintritt

Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.


# Sonstiges

Es gibt 🍿

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
