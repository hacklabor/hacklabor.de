---
title: Workshop Git - Verteilte Versionierung
tags: git source control vcs
author: Panzki
---

![git-workshop](/assets/blog/18-08-2018-git-workshop.png)

Wer kennt es nicht, man programmiert voller Eifer an seinem Projekt und 20 Minuten später geht gar nichts mehr? Wenn man doch nur eine Übersicht hätte was genau man geändert hat. Um diesem Problem in Zukunft aus dem Weg zu gehen bietet sich ein Tool zur Versionsverwaltung an. Das wohl bekannteste dieser Tools ist "Git".

Im Rahmen des Workshops gibt es eine Einführung in die Arbeit Git. Von der Installation, bis hin zum ersten kollaborativem Projekt. Das Format besteht aus kurzen Vortragspassagen zur Vermittlung der Konzepte und Workshopsessions zum Ausprobieren der Git Basics. Alle Fragen die euch während des Workshops in den Sinn kommen, können jeder Zeit gestellt werden.

Folgende Themen stehen u.a. auf der Agenda:

* Installation

* Konzepte

* Git alleine nutzen

* Git als Team nutzen

* Git Workflows

#### Wann?
18.08.2018 - ab 15-17 Uhr

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

#### Was muss ich mitbringen?
* Am besten bringst du deinen Laptop mit um am Workshop teilzunehmen. Solltest du keinen Laptop haben, auch kein Problem. Im Hacklabor gibt es einige Laptops, welche bei Bedarf ausgeliehen werden können.

* Gute Laune


#### Vortragender 

Panzki 

#### Eintritt

Die Veranstaltung ist kostenlos. Bring bitte trotzdem etwas Kleingeld für das eine oder andere mit.



Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)

