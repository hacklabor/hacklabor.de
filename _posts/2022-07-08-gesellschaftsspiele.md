---
title: Gesellschaftsspiele Nachmittag im Hacklabor
tags: event spaß Spiel public openhouse
author: Christoph 'ovnn'
---
# Gesellschaftsspiele Nachmittag im Hacklabor

Wir laden recht Herzlich am 6. August 2022 ab 14 Uhr bei Kaffe und Kuchen zu unserem nächsten Brettspiele Nachmittag ein.

Der Brettspiele Nachmittag Nachmittag steht diesmal unter dem Motto: "Mensch ärgere dich nicht"
Ein klassiker unter den Brettspielen und existiert so schon seit über 100 Jahren in Zahlreichen Varianten. Die Wurzeln des Spiels gehen laut Wikipedia auf das alte indische Spiel Pachisi zurück. Wer tiefer in die Geschichte eintauchen möchte, dem sei der ausführliche Wikipedia Eintrag empfohlen.

Natürlich besteht auch die möglichkeit andere Brettspiele zu spielen.

Die Veranstaltung ist wie immer kostenfrei.

Was?  - Gesellschaftsspiele Nachmittag im Hacklabor  
Wann? - Samstag 06.08.2022 ab 14 Uhr  
Wo?   - Hacklabor (im TGZ Haus1)  
        Hagenowerstr. 73 19061 Schwerin  
        
PS: [So kommst du zu uns](/standort/)
