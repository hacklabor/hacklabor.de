---
title: vi Workshop
tags: workshop
author: micha
---
![vi Workshop](/assets/blog/vi-workshop2017.png)

mit ":q" beendest du den besten* Editor der Welt.
Was du sonst noch alles mit ihm anstellen kannst, erfährst du am 11.12. 
ab 19 Uhr bei diesem Workshop im Hacklabor. Die Teilnahme ist kostenlos.
Du brauchst aber unbedingt deinen Rechner!

Wir freuen uns auf Euch!
Euer Team vom Hacklabor

PS: [So kommst du zu uns](/standort/)

*[Kommentare](https://www.facebook.com/events/1802264586481286/)
