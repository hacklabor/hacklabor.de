---
title: System Administrator Appreciation Day
tags: admin event sysadminday public talk
author: Gerd
---

![SysAdminDay](/assets/blog/2022-07-14-SysAdminDay.png)

Das Netzwerk ist sicher, auf dem PC sind die neuesten Updates installiert und der Drucker läuft ganz ohne Papierstau. Warum ist das so? Richtig, weil es engagierte Systemadmins gibt, die den Laden am Laufen halten.

Zeit, Danke zu sagen und zwar mit dem **System Administrator Appreciation Day, am Freitag den 29.07.2022 ab 19 Uhr nur im Hacklabor in Schwerin**.

Der SysAdminDay ist ein jährlicher Gedenktag, der von Ted Kekatos erfunden wurde und seit 2000 am letzten Freitag im Juli stattfindet. Kekatos wurde durch eine Anzeige von Hewlett-Packard inspiriert, in der einem Systemadministrator mit Blumen und Obstkörben von Mitarbeitern gedankt wird, weil er neue Drucker installiert hat.

Wir möchten alle Admins und Interessierte an diesem Abend ins Hacklabor im TGZ einladen, um ihre Arbeit zu ehren und sich in gemütlicher Runde auszutauschen. Weiterhin gibt es für alle Gäste ein **Freigetränk, sowie eine kostenlose Bratwurst vom Grill**.

Um 20 Uhr soll der **Vortrag** "Automatisierung ist des Admins bester Freund - eine Einführung in Ansible" dazu dienen, den Arbeitsalltag zukünftig etwas angenehmer zu gestalten.

Wir freuen uns auf euren Besuch!

#### Wann?
29.07.2022 - ab 19 Uhr

#### Wo?
Hacklabor /
TGZ Haus 1 /
Hagenower Straße 73 /
19061 Schwerin

#### Eintritt
Die Veranstaltung ist wie immer kostenlos.

