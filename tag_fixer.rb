#!/usr/bin/env ruby

# convert
#   tags: foo, bar, foobar
# to
#   tags: foo bar foobar
post_dir = '_posts/'

filenames = Dir.glob(post_dir + '*md')

filenames.each do |filename|
    post_content = File.read(filename)
    tag_line = post_content.each_line.to_a.grep(/^tags: /).first
    new_tag_line = tag_line.gsub(', ', ' ').downcase
    new_post_content = post_content.sub(tag_line, new_tag_line)
    File.open(filename, 'w') do |f|
      f.write(new_post_content)
    end
end
