#!/usr/bin/env ruby
#
# encoding: UTF-8

'''
based on https://github.com/qian256/qian256.github.io/blob/master/tag_generator.py
    copyright 2017 Long Qian
    Contact: lqian8@jhu.edu

This script creates tags for your Jekyll blog hosted by Github page.
No plugins required.
'''

require 'fileutils'

post_dir = '_posts/'
tag_dir = 'blog/tag/'

filenames = Dir.glob(post_dir + '*md')

total_tags = []
filenames.each do |filename|
  begin
    post_content = File.read(filename, encoding: 'UTF-8')
    post_content.each_line do |line|
        key, *current_tags = line.strip.split(':')
        if key == 'tags'
            total_tags += current_tags.join(' ').split(' ').map{|t| t.strip.downcase }
            next
        end
    end
  rescue Encoding::InvalidByteSequenceError
      $stderr.write("failed to read #{filename}\n")
      raise
  end
end

total_tags.compact!
tag_stats = total_tags.inject({}) do |memo, tag|
  cnt = memo[tag] || 0
  cnt += 1
  memo[tag] = cnt
  memo
end
tag_stats.to_a.sort_by{|tag, cnt| "#{5000-cnt} #{tag}" }.each do |tag, cnt|
  puts "#{cnt}x #{tag}"
end
total_tags.uniq!
total_tags.sort!

Dir.glob(tag_dir + '*.md').each do |file|
  FileUtils.rm(file)
end

FileUtils.mkdir_p(tag_dir)

for tag in total_tags
    tag_filename = tag_dir + tag + '.md'
    File.open(tag_filename, 'a') do |f|
        write_str = "---
layout: tagpage
title: \"Blogposts getagged mit: #{tag} \"
tag: #{tag}
robots: noindex
---"
        f.write(write_str)
    end
end
puts("#{total_tags.length} Tags generated")
