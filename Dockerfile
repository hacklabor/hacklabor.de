# used for local development
FROM ruby:2.5

# set locale to UTF-8
ENV RUBYOPT -EUTF-8

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN gem install bundler \
    && bundle install

EXPOSE 4000
CMD jekyll serve --destination public --host 0.0.0.0