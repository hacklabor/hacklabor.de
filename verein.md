---
layout: page
title: Hackspace Schwerin e.V.
permalink: /verein/
---

Das Hacklabor wird vom Verein Hackspace Schwerin e.V. betrieben. Die Gründung des Vereins erfolgte am 26. Februar 2016. Beim Amtsgericht Schwerin hat der Verein die Registernummer 10234. Die [Satzung](/satzung.pdf) kannst du [hier](/satzung.pdf) nachlesen.

## Vorstand

Der Vorstand besteht aus folgenden Personen:

-   Gerd Kant
-   [Matthias Manow](https://twitter.com/moeses)
-   Stefan Hausmann

{% include mitglied-werden.html %}

## Spenden

Der Verein ist gemeinnützig und Spenden an den Verein können steuerlich geltend gemacht werden.

Zur Unterstützung gibt es die [Kaufkröte](https://www.Kaufkroete.de/4224kroeten) mit der man unter [Kaufkroete.de/4224kroeten](https://www.Kaufkroete.de/4224kroeten) viele Shops besuchen kann. Dafür bekommen wir Provision ohne das sich der Kaufpreis erhöht.

Außerdem kann man unsere [Wünsche](https://www.amazon.de/gp/registry/wishlist/2W8SE6FZMKAZE/ref=as_li_ss_tl?ie=UTF8&linkCode=ll2&linkId=126aad26a1031ff4751c1fe3e060ea76) bei Amazon erfüllen.

## Kontoverbindung

Kontoinhaber: Hackspace Schwerin e.V.  
IBAN: DE76 8306 5408 0004 9491 70

## Sponsoring

Bitte [sprechen Sie uns an](/kontakt/).

## Logos

![Kleines Logo Hacklabor](/assets/img/logo/Logo_small_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_small_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_small_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_small_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_small_black.svg)

![Mittleres Logo Hacklabor](/assets/img/logo/Logo_medium_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_medium_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_medium_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_medium_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_medium_black.svg)

![Großes Logo Hacklabor](/assets/img/logo/Logo_Large_white.svg.png)  

-   PNG [weiß auf transparent](/assets/img/logo/Logo_Large_white.svg.png) / [schwarz auf weiß](/assets/img/logo/Logo_Large_black.svg.png)
-   SVG [weiß auf transparent](/assets/img/logo/Logo_Large_white.svg) / [schwarz auf weiß](/assets/img/logo/Logo_Large_black.svg)

## Pressespiegel

- 2016-01-09 - SVZ - [Pfiffige Köpfe für Hackerspace gesucht](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/pfiffige-koepfe-fuer-hackerspace-gesucht-id12398366.html)
- 2016-03-10 - NDR 1 Radio MV - [Ortszeit](https://www.youtube.com/watch?v=agCLQqe1FEU)
- 2016-09-22 - SVZ - [Technik-Freaks wollen helfen](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/technik-freaks-wollen-helfen-id14903056.html)
- 2016-09-25 - dieschweriner.de - [Wenn man ins Hackspace geht...](https://dieschweriner.de/diefreizeit/wenn-man-ins-hackspace-geht-6828)
- 2016-10-11 - SVZ - [Freies WLAN im Landratsamt](https://www.svz.de/lokales/ludwigsluster-tageblatt/freies-wlan-im-landratsamt-id15064881.html)
- 2016-12-11 - NDR MV - Nordmagazin\*
- 2017-03-17 - SVZ - [Offenem Netz ein Stück näher](https://www.svz.de/lokales/ludwigsluster-tageblatt/offenem-netz-ein-stueck-naeher-id16377086.html)
- 2017-04-18 - SchwerinTV - MV Uncut - [Sendung #7](https://www.youtube.com/watch?v=VHaMT6966Tg)
- 2017-10-12 - SVZ - [Wissenschaft zum Anfassen](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/wissenschaft-zum-anfassen-id18051691.html)
- 2017-10-15 - SVZ - [Lennart trickst den Automaten aus](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/lennart-trickst-den-automaten-aus-id18076661.html)
- 2018-06-04 - SVZ - [Mit Code die Welt verbessern](https://www.svz.de/regionales/mecklenburg-vorpommern/mit-code-die-welt-verbessern-id20023272.html)
- 2018-09-04 - Unternehmerverband Norddeutschland Mecklenburg-Schwerin e.V. - [UV-Ausbildertrainingscamp 2018](https://unternehmerverbaende-mv.com/aktuell-mecklenburg/regionale-news/schwerin/2541-uv-ausbildertrainingscamp-2018.html)
- 2018-10-19 - SVZ - [Ansehen, anfassen, ausprobieren](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/Samstag-startet-die-lange-Nacht-des-Wissens-mit-mehr-als-150-Veranstaltungen-in-Schwerin-id21379002.html)
- 2018-10-19 - Schwerin Lokal - [Nacht des Wissens am Samstagabend](https://schwerin-lokal.de/nacht-des-wissens-am-samstagabend/)
- 2018-12-28 - SVZ - [Hacken im Verein](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/Hacklabor-Schwerin-Matthias-Manow-engagiert-sich-ehrenamtlich-id22086107.html)
- 2019-03-19 - Fernsehen in Schwerin - [3 Jahre Hacklabor](https://mmv-mediathek.de/play/26934-3-jahre-hacklabor.html)
- 2019-04-01 - SVZ - [Angehende IT-Spezialisten feilen an der Zukunftsstadt](https://www.svz.de/lokales/rostock/Jugend-hackt-MV-IT-Spezialisten-feilen-an-der-Zukunftsstadt-id23188567.html)
- 2019-05-02 - NDR 1 Radio MV - [Ortszeit regional - Ehrenamt](https://www.ndr.de/radiomv/Ortszeit-Mecklenburg-Studio-Schwerin-Ehrenamt,audio511638.html)
- 2020-07-22 - SVZ - [Verfassungsklage gegen MVs neues Polizeigesetz](https://www.svz.de/29053532)
- 2020-11-07 - BLITZ - Begeisterung für MINT-Fächer (Technolympiade 2020)\*
- 2021-06-10 - SVZ - [Helios-Kliniken und SVZ wählen Gewinner von 10.000 für Zehn](https://www.svz.de/32535182)
- 2021-06-11 - OZ - Dreidimensionale Kunst in Wismar\*
- 2021-08-26 - SVZ - [Zehn Vereine können jubeln](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/Gewinner-der-Aktion-10-000-fuer-Zehn-bekommen-Foerderung-id33418852.html)
- 2021-10-17 - SVZ - [Computer-Experten erklären, wer Hacker wirklich sind](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/Schweriner-Hacker-diskutieren-ueber-Cyberangriff-auf-Schwerin-id34015182.html)
- 2021-10-18 - SVZ - [Linus bringt Wäscheklammern zum Leuchten](https://www.svz.de/lokales/zeitung-fuer-die-landeshauptstadt/Linus-bringt-Waescheklammern-zum-Leuchten-id34015172.html)
- 2022-04-01 - SVZ - [Stadt schafft Coworking-Plätze für ukrainische Flüchtlinge](https://www.svz.de/lokales/schwerin/artikel/innovationszentrum-schwerin-bietet-coworking-plaetze-fuer-ukrainer-22901684)
- 2022-08-25 - SVZ - [Warum der Begriff Hackerangriff falsch sein soll](https://www.svz.de/lokales/schwerin/artikel/hacklabor-schwerin-erklaert-was-hacken-eigentlich-bedeutet-42845278)
- 2022-10-22 - SVZ - [Eingeladen zur Nacht des Wissens](https://www.genios.de/presse-archiv/artikel/SVZ/20221022/eingeladen-zur-nacht-des-wissens/23-153972656.html)\*
- 2023-05-27 - SVZ - [Mit Open Bike Sensor auf Gefahrensuche im Verkehr in Schwerin](https://www.svz.de/lokales/schwerin/artikel/schwerin-mit-open-bike-sensor-auf-gefahrensuche-im-verkehr-44804221)
- 2023-09-06 - SVZ - [Mehr Sicherheit für Radfahrer in Schwerin: Projekt Open Bike Sensor nun kostenlos](https://www.svz.de/lokales/schwerin/artikel/fahrrad-in-schwerin-projekt-open-bike-sensor-wird-gefoerdert-45450368)
- 2023-09-26 - NDR - [Open Bike Sensor: Neues Projekt für mehr Verkehrssicherheit](https://www.ndr.de/fernsehen/sendungen/nordmagazin/Open-Bike-Sensor-Neues-Projekt-fuer-mehr-Verkehrssicherheit,nordmagazin111314.html)
- 2024-01-29 - hauspost - [Radeln für Daten – Schwerins Weg zur Fahrradstadt](https://www.hauspost.de/artikel/11990.html)
- 2024-02-18 - BLITZ - [Schwerins Weg zur Fahrradstadt](https://www.blitzverlag.de/wp-content/uploads/2024/02/20240218_03-2.pdf#page=3)

\* Depubliziert
