---
title: "MOiN Treffen - oder: Was macht MOiN "
tags: chaostreff-flensburg,chaotikum,  Hackspace Rostock, hacklabor
author: moe
---

[![MOiN Wiki](/assets/blog/Screenshot_2020-03-09AssemblyMOiN-36C3Wiki.png)](https://events.ccc.de/congress/2019/wiki/index.php/Assembly:MOiN)




Jeder kennt es: Es sind ja noch 6++ Monate zum naechsten Congress, noch brauche ich mir keinen Kopf machen 

Aber: Es läuft dann ist es immer so das es den Leuten im Oktober einfaellt das man was für den Congress machen koennte.

Nur: Wie soll man die Plaene jetzt noch umsetzen ? 

Und um das Problem zu lösen will man sich schon weit vorher Treffen um Ideen zu sammeln. 

__Dies ist das 1. Treffen der Mehreren Orten im Norden ( MOiN).__ 

Angestrebt wir ein Wochenende in Schwerin, bei denen sich die Leute mal naeher kennenlernen sollen, und wenn dann noch Ideen fuer den #37C3 dabei rauskommen um so besser =) 


#### Wann?
24.04.2020 - ab ~16 Uhr bis 26.04.2020

#### Wo?
Hacklabor Schwerin
Hagenower Straße 73
19061 Schwerin

Wir freuen uns auf Dich!

PS: [So kommst du zu uns](/standort/)
