---
title: Projekt Opennet/Landkreis Ludwigslust Parchim
tags: offen, opening
author: rene
---


Am 10.10.2016 erfolgte der Startschuss für ein Gemeinschaftsprojekt von Opennet, 
dem Schweriner Hacklabor und dem Landkreis Parchim/ Ludwigslust.
Geplant ist der Aufbau eines dezentralen Netzwerks für die Bereitstellung 
offener Internetzugangspunkte. Über dieses ist eine zeitlich unbegrenzte 
Nutzung des Internets ohne Registrierung möglich.
Dieses Angebot richtet sich an die Bürger und Touristen im Landkreis.
Die Verwendung ist an öffentlichen Plätzen, Schulen und Gebäuden im kommunalen Bereich vorgesehen.
Die Infrastruktur (Server und Software) wird hierfür von Opennet zur Verfügung gestellt.
Die Bereitstellung der Hardware erfolgt durch den Landkreis Parchim/ Ludwigslust.
Im ersten Schritt erfolgte nun der Aufbau der ersten offenen Zugangspunkte im 
Bürgerbüro Parchim sowie im Landratsamt Ludwigslust.
In der zweiten Phase erfolgt dann die Vernetzung entfernter Standorte über ein 5GHz Netzwerk.

Auch die Presse berichtet:

  * http://www.svz.de/lokales/ludwigsluster-tageblatt/freies-wlan-im-landratsamt-id15064881.html