---
layout: page
title: Standort
permalink: /standort/
---

Das Hacklabor befindet sich im Haus 1 des [TGZ](https://www.tgz-mv.de/) in Schwerin.  
Die Adresse lautet:

Hagenower Straße 73  
19061 Schwerin

[53.6011, 11.4183](geo:53.6011,11.4183)

Adresse öffnen in [OSM](https://osm.org/go/0NAH7ckyf--?layers=HN&m=&node=3948563704)/[Google](https://goo.gl/maps/HgK3iCKfRwn).

## Parkplätze

Auf dem Gelände des TGZ befinden sich zahlreiche Parkplätze für Autos. Dein Fahrrad kannst du direkt vor dem Hacklabor oder gegenüber im Fahrradständer abstellen.

## ÖPNV

Das TGZ erreichst du prima per Bus oder Straßenbahn. Wir empfehlen folgende Haltestellen:

-   Wilhelm-Hennemann-Straße (ehemals Technologiezentrum) (Bus 7) - [Fahrpläne](https://nahverkehr-schwerin.de/fahrplan/fahrplan_2018-2019/aushang/index.html#W)
-   alternativ: Technologiepark (Bus 7) - [Fahrpläne](https://nahverkehr-schwerin.de/fahrplan/fahrplan_2018-2019/aushang/#R)
-   Gartenstadt (Tram 1,2,4, Bus 19) - [Fahrpläne](https://nahverkehr-schwerin.de/fahrplan/fahrplan_20178-2019/aushang/#G)

### Fahrplanauskunft

-   [Hinfahrt](https://efa.vmv-mbh.de/vmv/XSLT_TRIP_REQUEST2?language=de&sessionID=0&itdLPxx_transpCompany=vmv&execInst=readOnly&lineRestriction=400&ptOptionsActive=1&place_destination=Schwerin&name_destination=Hagenower%20Strasse%2073&type_destination=address&)
-   [Rückfahrt](https://efa.vmv-mbh.de/vmv/XSLT_TRIP_REQUEST2?language=de&sessionID=0&itdLPxx_transpCompany=vmv&execInst=readOnly&lineRestriction=400&ptOptionsActive=1&place_origin=Schwerin&name_origin=Hagenower%20Strasse%2073&type_origin=address)

### Abfahrtsmonitor

<ul id="listefahrten">
	<li>Bitte warte kurz oder aktiviere Javascript :)</li>
</ul>

<script src="/assets/js/mustache.min.js"></script>
{% raw  %}
<script id="fahrtTpl" type="text/template">
	{{#.}}
	<li>
		<span class="time">{{UhrzeitAusgabe}}</span><br>
		<span class="type">{{Typ}}</span> <span class="number">{{Nummer}}</span> <span class="dest">{{Ziel}}</span><br>
		<span class="stop">{{Haltestelle}}</span>
	</li>
	{{/.}}
</script>
{% endraw  %}
