---
layout: page
title: Projekte
permalink: /projekte/
---

Diese Liste bietet einen Überblick über die auf gitlab.com öffentlich einsehbaren Projekte des Hacklabors.

## Die 20 zuletzt bearbeiteten Projekte


<div class="card-columns">
{% assign doclist = site.data.gitlabprojects | sort: 'last_activity_at' | reverse %}
{% for item in doclist limit:20 %}
  {% if item.visibility == 'public' %}
    <div class="card">
      {% if item.avatar_url %}
        <a href="{{ item.web_url }}"><img class="figure-img" src="{{ item.avatar_url }}" alt="{{ item.name }}"></a>
      {% endif %}
      <div class="card-block">
        <h3 class="card-title"><a href="{{ item.web_url }}">{{ item.name }}</a></h3>
        <p class="card-text">{{ item.description }}</p>
      </div>
    </div>
  {% endif %}
{% endfor %}
</div>

## Länger ruhende oder abgeschlossene Projekte

<div class="card-columns">
{% assign doclist = site.data.gitlabprojects | sort: 'last_activity_at' | reverse %}
{% for item in doclist offset:20 %}
  {% if item.visibility == 'public' %}
    <div class="card">
      {% if item.avatar_url %}
        <a href="{{ item.web_url }}"><img class="figure-img" src="{{ item.avatar_url }}" alt="{{ item.name }}"></a>
      {% endif %}    
      <div class="card-block">
        <h3 class="card-title"><a href="{{ item.web_url }}">{{ item.name }}</a></h3>
        <p class="card-text">{{ item.description }}</p>
      </div>
    </div>
  {% endif %}
{% endfor %}
</div>
