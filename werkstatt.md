---
layout: page
title: Offene Werkstatt
permalink: /werkstatt/
---

Neben dem Hackspace haben wir auch eine offene Werkstatt, die jedem der ein Projekt umsetzen möchte zur Verfügung steht. Hier kannst du dann bei uns an deinem Projekt arbeiten und wir geben Unterstützung beim Selbermachen.

Die Werkstatt ist neben dem Hackspace auch immer mittwochs und freitags ab 1900 Uhr geöffnet. Zusätzlich ist die Werkstatt regelmäßig einmal im Monat an einem Samstag geöffnet, den nächsten Termin findest du hier: https://hacklabor.de/termine/

Ganz gleich ob du etwas reparieren oder etwas neues erschaffen willst, du kannst bei uns Holz, Plexiglas, Elektronik und Metall bearbeiten. Wie wäre es z.B. mit einem Nistkasten oder einer schicken LED-Installation für zu Hause?


## Hier einige Beispiele von bereits in der Werkstatt umgesetzten Projekten:


![3D Druck](/assets/img/werkstatt/werkstatt1.jpg) ![Weihnachtssterne](/assets/img/werkstatt/werkstatt2.jpg) ![Hacklabor Info System](/assets/img/werkstatt/werkstatt3.jpg) ![Gelöteted Eule für die NdW](/assets/img/werkstatt/werkstatt4.jpg) ![CNC gefrästes Werkstattschild](/assets/img/werkstatt/werkstatt6.jpg) ![Trophäe](/assets/img/werkstatt/werkstatt8.jpg)  
