jQuery(function() {
  	$.ajax({
		url: "/api/space/v1/",
		dataType: "json",
		type : "GET",
		success : function(space) {
			if (space.state.open) {
				$('body').addClass('openspace');
			}
		}
    });
    
    if ( $("#abfahrtsmonitor").length) {
        $.getJSON('/api/abfahrtsmonitor/', function(data) {
    	    var template = $('#fahrtTpl').html();
    	    var html = Mustache.to_html(template, data);
    	    $('#listefahrten').html(html);
	    });
    }
});
